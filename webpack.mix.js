const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// ADMIN GENERAL
// General CSS files
mix.styles([
    'resources/assets/dashboard/css/layout/header/base/light.css',
    'resources/assets/dashboard/css/layout/header/menu/light.css',
    'resources/assets/dashboard/css/layout/aside/dark.css',
    'resources/assets/dashboard/css/layout/brand/dark.css',
    'resources/assets/dashboard/css/plugins/global/plugins.bundle.css',
    'resources/assets/dashboard/css/style.bundle.css',
    'resources/assets/dashboard/css/custom.css',
    'resources/assets/site/css/vendor/icons.min.css',
], 'public/assets/dashboard/css/all.css');

// General JS files
mix.scripts([
    'resources/assets/dashboard/js/plugins/global/plugins.bundle.js',
    'resources/assets/dashboard/js/plugins/custom/prismjs/prismjs.bundle.js',
    'resources/assets/dashboard/js/scripts.bundle.js',
    'resources/assets/dashboard/js/global.scripts.js',
], 'public/assets/dashboard/js/all.js');

// Auth module CSS files
mix.styles([
    'resources/assets/dashboard/css/style.bundle.css',
    'resources/assets/dashboard/css/auth/login.css',
], 'public/assets/dashboard/css/auth.css');

mix.copyDirectory('resources/assets/dashboard/images', 'public/assets/images'); //TODO: move to dashbaord and handle

mix.copyDirectory('resources/assets/dashboard/fonts', 'public/assets/dashboard/fonts');


// SITE
// CSS files
mix.styles([
    'resources/assets/site/css/vendor/animation.min.css',
    'resources/assets/site/css/vendor/bootstrap.min.css',
    'resources/assets/site/css/vendor/cookie-notice.min.css',
    'resources/assets/site/css/vendor/gallery.min.css',
    'resources/assets/site/css/vendor/icons.min.css',
    'resources/assets/site/css/vendor/slider.min.css',
    'resources/assets/site/css/default.css',
    'resources/assets/site/css/main.css',
    'resources/assets/site/css/theme-green.css',
], 'public/assets/site/css/all.css');

// General JS files
mix.scripts([
    'resources/assets/site/js/vendor/jquery.min.js',
    'resources/assets/site/js/vendor/jquery.easing.min.js',
    'resources/assets/site/js/vendor/jquery.inview.min.js',
    'resources/assets/site/js/vendor/popper.min.js',
    'resources/assets/site/js/vendor/bootstrap.min.js',
    'resources/assets/site/js/vendor/ponyfill.min.js',
    'resources/assets/site/js/vendor/slider.min.js',
    'resources/assets/site/js/vendor/animation.min.js',
    'resources/assets/site/js/vendor/progress-radial.min.js',
    'resources/assets/site/js/vendor/bricklayer.min.js',
    // 'resources/assets/site/js/vendor/gallery.min.js',
    'resources/assets/site/js/vendor/shuffle.min.js',
    'resources/assets/site/js/vendor/cookie-notice.min.js',
    'resources/assets/site/js/vendor/particles.min.js',
    'resources/assets/site/js/main.js',
], 'public/assets/site/js/all.js');

mix.copyDirectory('resources/assets/site/images', 'public/assets/site/images');

mix.copyDirectory('resources/assets/site/css/fonts', 'public/assets/fonts');