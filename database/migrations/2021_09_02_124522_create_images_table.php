<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('imageable_id');

            $table->string('imageable_type');
            $table->string('filename');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->smallInteger('order')->default(99);

            $table->timestamps();
            $table->string('deletion_token')->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Storage::disk('public')->deleteDirectory('images');
    }
}
