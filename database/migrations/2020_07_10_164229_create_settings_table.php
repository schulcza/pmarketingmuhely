<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            
            $table->string('name');
            $table->string('keyword');
            $table->text('value')->nullable();
            $table->string('type');
            $table->string('group');
            $table->text('help_text')->nullable();

            $table->timestamps();
            $table->string('deletion_token')->default(0);
            $table->softDeletes();

            $table->unique(['name', 'deletion_token']);
            $table->unique(['keyword', 'deletion_token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
