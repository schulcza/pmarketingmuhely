<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();

            $table->string('title');
            $table->string('slug');
            $table->unsignedBigInteger('featured_image_id')->nullable();
            $table->unsignedBigInteger('image_id')->nullable();
            $table->longText('body')->nullable();
            $table->boolean('featured')->default(false);
            $table->string('url')->nullable();
            $table->json('categories')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
