<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create module permissions
        foreach (config('core.permissions.modules') as $key => $permission) {
            if (!Permission::where('name', $permission)->first()) {
                Permission::create(['name' => $permission]);
            }
        }

        // Create user role and assign existing permissions
        $currentPermissions = Permission::all();

        if (!$userRole = Role::where('name', 'user')->first()) {
            $userRole = Role::create(['name' => 'user']);
        }
        $userRole->givePermissionTo($currentPermissions);

        // Create module permissions
        foreach (config('core.permissions.base') as $key => $basePermission) {
            if (!Permission::where('name', $basePermission)->first()) {
                Permission::create(['name' => $basePermission]);
            }
        }

        Permission::firstOrCreate(['name' => 'view users']);
        Permission::firstOrCreate(['name' => 'view settings']);

        // Create admin role and assign all permissions
        $allPermissions = Permission::all();
        if (!$adminRole = Role::where('name', 'admin')->first()) {
            $adminRole = Role::create(['name' => 'admin']);
        }
        $adminRole->givePermissionTo($allPermissions);
    }
}
