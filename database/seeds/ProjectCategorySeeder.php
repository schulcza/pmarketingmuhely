<?php

use App\Models\ProjectCategory;
use Illuminate\Database\Seeder;

class ProjectCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projectCategories = [
            [
                'name' => 'Weboldal'
            ],
            [
                'name' => 'Arculat'
            ],
            [
                'name' => 'Logo'
            ],
            [
                'name' => 'Marketing'
            ]
        ];

        foreach ($projectCategories as $projectCategory) {
            ProjectCategory::updateOrCreate($projectCategory);
        }
    }
}
