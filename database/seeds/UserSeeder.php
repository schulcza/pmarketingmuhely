<?php

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => Str::random(10),
            'email' => config('app.admin_email'),
            'password' => \Hash::make(config('app.password')),
            'email_verified_at' => \Carbon\Carbon::now(),
            'remember_token' => '',
        ]);

        $admin->assignRole('admin');

        $user = User::create([
            'name' => Str::random(10),
            'email' => config('app.user_email'),
            'password' => \Hash::make(config('app.password')),
            'email_verified_at' => \Carbon\Carbon::now(),
            'remember_token' => '',
        ]);

        $user->assignRole('user');
    }
}
