<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $settings = [
            [
                'name' => 'Logo',
                'keyword' => 'logo',
                'value' => null,
                'type' => 'file',
                'group' => 'Base data',
                'help_text' => ''
            ],
            [
                'name' => 'Default e-mail address',
                'keyword' => 'admin_email',
                'value' => 'admin@admin.com',
                'type' => 'text',
                'group' => 'Email addresses',
                'help_text' => ''
            ],
            [
                'name' => 'Email cc-s (comma separated)',
                'keyword' => 'cc_emails',
                'value' => 'ccemail1@email.com, ccemail2@email.com',
                'type' => 'text',
                'group' => 'Email addresses',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Térkép API kulcs',
                'keyword' 	=> 'map_api_key',
                'value' 	=> '',
                'type'		=> 'text',
                'group'		=> 'API kulcsok',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Weboldal cím',
                'keyword'	=> 'meta_title',
                'value'		=> '',
                'type'		=> 'text',
                'group'		=> 'Meta adatok',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Meta leírás (120-158 karakter)',
                'keyword'	=> 'meta_description',
                'value'		=> '',
                'type'		=> 'textarea',
                'group'		=> 'Meta adatok',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Kulcsszavak (vesszővel elválasztva)',
                'keyword'	=> 'keywords',
                'value'		=> '',
                'type'		=> 'textarea',
                'group'		=> 'Meta adatok',
                'help_text' => ''
            ],
            [
                'name' 		=> 'OG kép (social media megosztásnál jelenik meg)',
                'keyword'	=> 'og_image',
                'value'		=> null,
                'type'		=> 'file',
                'group'		=> 'Meta adatok',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Facebook oldal URL',
                'keyword'	=> 'facebook_url',
                'value'		=> '',
                'type'		=> 'text',
                'group'		=> 'Social linkek',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Youtube csatorna URL',
                'keyword'	=> 'youtube_url',
                'value'		=> '',
                'type'		=> 'text',
                'group'		=> 'Social linkek',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Slider 1 címsor',
                'keyword'	=> 'slide1_title',
                'value'		=> '',
                'type'		=> 'text',
                'group'		=> 'Tartalom',
                'help_text' => ''
            ],
            [
                'name' 		=> 'Slider 1 tartalom',
                'keyword'	=> 'slide1_content',
                'value'		=> '',
                'type'		=> 'text',
                'group'		=> 'Tartalom',
                'help_text' => ''
            ]
        ];

        foreach ($settings as $setting) {
            Setting::updateOrCreate($setting);
        }
    }
}
