<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Project;
use Faker\Generator as Faker;
use App\Models\ProjectCategory;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'featured' => $faker->boolean,
        'image_id' => null,
        'url' => $faker->url,
        'categories' => [ProjectCategory::inRandomOrder()->first()->id]
    ];
});
