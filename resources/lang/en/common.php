<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'name' => 'Name',
    'add_new' => 'Create new',
    'search' => 'Search',
    'keyword' => 'Keyword',
    'operations' => 'Operations',
    'actions' => 'Actions',
    'logout' => 'Log out',
    'are_you_sure_delete' => 'Are you sure to delete?',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'yes' => 'Yes',
    'yes_delete' => 'Yes, delete',
    'status' => 'Statis',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'deleted' => 'Deleted',
    'show' => 'Show',
    'choose' => 'Choose',
    'all' => 'All',
    'created_at' => 'Created at',
    'created_by' => 'Created by',
    'publish_date' => 'Published at',
    'please_wait' => 'Please wait...',
    'day' => 'day',
    'change' => 'Change',
    'search_delete' => 'Delete search',
    'piece_short' => 'db',
    'my_profile' => 'My profile',
    'my_profile_desc' => 'Profile settings',
    'back' => 'Back',
    'actions' => 'Actions',
    'create' => 'Create',
    'edit' => 'Edit',
    'update' => 'Update',
    'are_you_sure' => 'Are you sure?',
    'no_items_found' => 'No items found',
    'created' => 'Successfully created',
    'saved' => 'Saved successfully',
    'removed' => 'Successfully removed',
    'edited_by' => 'Edited by',
    'updated_at' => 'Updated at',
    'updated_by' => 'Updated by',
    'reset' => 'Reset',
	'unable_to_save' => 'Unable to save',
	'unable_to_delete' => 'Unable to delete'
];
