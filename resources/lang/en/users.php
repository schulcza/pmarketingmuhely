<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User related Language Lines
    |--------------------------------------------------------------------------
    */

    'base_data' => 'Base data',
    'users' => 'Users',
    'user' => 'User',
    'edit_user' => 'Edit user',
    'name' => 'Name',
    'username' => 'Username',
    'password' => 'Password',
    'email' => 'Email address',
    'change_password' => 'Change password',
    'deleted_at' => 'Deleted at',
];
