<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User related Language Lines
    |--------------------------------------------------------------------------
    */

    'title' => 'Title',
    'slug' => 'Slug',
    'image' => 'Image',
    'body' => 'Body',
    'featured' => 'Featured',
    'url' => 'URL',
    'categories' => 'Categories',
];
