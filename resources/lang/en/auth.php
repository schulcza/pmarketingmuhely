<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => 'Login',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'password' => 'Password',
    'change_password' => 'Change password',
    'password_confirm' => 'Password confirmation',
    'password_confirmation' => 'Password confirmation',
    'reset_password' => 'Reset password',
    'email' => 'E-mail address',
    'login_title' => 'Welcome',
    'forgot_password' => 'Forgot password?',
    'enter_your_email_to_reset_password' => 'Enter your email to reset password',
    'ask_reset_submit' => 'Reset password',
    'cancel' => 'Cancel',
    'new_password_title' => 'Set new password',
    'new_password_submit' => 'Save password',
    'reset_password_title' => 'Reset password',
    'send_password_reset_link' => 'Send password reset link',

    'passwords.user' => 'User not found!',
    'passwords.sent' => 'Reset email sent!',
    'passwords.throttled' => 'Reset email already sent!',
];
