<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User related Language Lines
    |--------------------------------------------------------------------------
    */

    'title' => 'Title',
    'slug' => 'Slug',
    'excerpt' => 'Excerpt',
    'featured_image' => 'Featured image',
    'body' => 'Body',
    'featured' => 'Featured',
    'url' => 'URL',
    'categories' => 'Categories',
];
