@extends('layouts.site')

@section('content')

{{-- @include('site.sections.hero-full') --}}
@include('site.sections.hero')
@include('site.sections.about')
@include('site.sections.services')
{{-- @include('site.sections.services-2') --}}
{{-- TODO References --}}
@include('site.sections.projects')
{{-- @include('site.sections.fun-facts') --}}
@include('site.sections.blog')
@include('site.sections.newsletter')
@include('site.sections.contact')

@endsection