@extends('layouts.site')

@section('content')
	<!-- Hero -->
	<section id="slider" class="hero p-0 odd featured">
		<div class="swiper-container no-slider slider-h-60">
			<div class="swiper-wrapper">

				<!-- Item 1 -->
				<div class="swiper-slide slide-center">
					
					<img src="{{ $service->getImage('normal') }}" class="full-image" data-mask="70">
					
					<div class="slide-content row text-center">
						<div class="col-12 mx-auto inner">
							{{-- <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
								{{ $service->name }}
							</h1> --}}
							{{-- <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/#demos">Home</a></li>
									<li class="breadcrumb-item"><a href="/#pages">Pages</a></li>
									<li class="breadcrumb-item active" aria-current="page">Single Portfolio</li>
								</ol>
							</nav> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Service content-->
	<section id="content" class="section-1 single">
		<div class="container">
			<div class="row">
				<!-- Main -->
				<main class="col-12 col-lg-8 p-0">
					<div class="row">
						<div class="col-12 align-self-center">
							<h3 class="featured mt-0 ml-0">
								{{ $service->name }}
							</h3>
							<p>{!! $service->excerpt !!}</p>

							<p>{!! $service->body !!}</p>
						</div>
					</div>        
				</main>

				<!-- Sidebar -->
				<aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">                    
					<div class="row">
						<div class="col-12 align-self-center text-left">
							<!-- Widget [services] -->
							<div class="item widget-services">
								<h4>
									Továbi szolgáltatásaink
								</h4>
								<ul class="list-group list-group-flush widget-services">
									@foreach ($services as $service)
										<li class="list-group-item">
											<a href="{{ route('services.show', $service->slug) }}">
												<i class="icon {{ $service->icon }}"></i>
												<h5>
													{{ $service->name }}
												</h5>
											</a>
										</li>
									@endforeach
								</ul>
							</div>

						</div>
					</div>
				</aside>
			</div>
		</div>
	</section>
@endsection
