<section id="about" class="section-1 highlights image-right">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 align-self-center text-center text-md-left">
				<div class="row intro">
					<div class="col-12 p-0">
						<h2 class="featured alt">About Us</h2>
						<p>For over 8 years we have been developing digital solutions for companies looking to gain better visibility on the internet.</p>
					</div>
				</div>
				<div class="row items">
					<div class="col-12 p-0">
						<div class="row item">
							<div class="col-12 col-md-2 align-self-center">
								<i class="icon icon-badge"></i>
							</div>
							<div class="col-12 col-md-9 align-self-center">
								<h4>Quality</h4>
								<p>Everything we do has the commitment of a well trained and motivated team.</p>
							</div>  
						</div>
						<div class="row item">
							<div class="col-12 col-md-2 align-self-center">
								<i class="icon icon-briefcase"></i>
							</div>
							<div class="col-12 col-md-9 align-self-center">
								<h4>Experience</h4>
								<p>Focused on results we seek to raise the level of our customers.</p>
							</div>  
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<a href="/assets/site/images/pmarketingmuhely_logo_cropped.png">
					<img src="/assets/site/images/pmarketingmuhely_logo_cropped.png" class="fit-image" alt="About Us">
				</a>
			</div>
		</div>
	</div>
</section>