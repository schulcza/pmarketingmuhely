<section id="features" class="section-2 odd offers featured">
	<div class="container">
		<div class="row justify-content-center text-center items">
			@foreach ($services as $service)
				<div class="col-12 col-md-6 col-lg-4 item">
					<div class="card no-hover">
						<i class="icon icon-globe"></i>
						<h4>
							{{ $service->name }}
						</h4>
						<p>{{ $service->excerpt }}</p>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>