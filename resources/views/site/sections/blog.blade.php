<section id="news" class="section-4 carousel showcase news">
	<div class="overflow-holder">
		<div class="container">
			<div class="row intro">
				<div class="col-12 col-md-9 align-self-center text-center text-md-left">
					<h2 class="featured">Latest News</h2>
					<p>Every week we publish exclusive content on various topics.</p>
				</div>
				<div class="col-12 col-md-3 align-self-end">
					<a href="{{ route('posts.index') }}" class="btn mx-auto mr-md-0 ml-md-auto primary-button"><i class="icon-grid"></i>ÖSSZES BEJEGYZÉS</a>
				</div>
			</div>
			<div class="swiper-container mid-slider items">
				<div class="swiper-wrapper">
					@foreach ($posts as $post)
						<div class="swiper-slide slide-center item">
							<div class="row card p-0 text-center">
								<div class="image-over">
									<img src="{{ $post->getImage('thumbnail') }}" alt="{{ $post->title }}">
								</div>
								<div class="card-caption col-12 p-0">
									<div class="card-body">
										<a href="{{ route('posts.show', $post->slug) }}">
											<h4 class="m-0">
												{{ $post->title }}
											</h4>
										</a>
									</div>
									<div class="card-footer d-lg-flex align-items-center justify-content-center">
										{{-- <a href="#" class="d-lg-flex align-items-center"><i class="icon-tag"></i>
											Category
										</a> --}}
										<a href="#" class="d-lg-flex align-items-center"><i class="icon-clock"></i>
											{{ dateFormat($post->created_at) }}
										</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
	</div>
</section>