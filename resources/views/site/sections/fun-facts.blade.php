<section id="funfacts" class="section-3 odd counter funfacts">
	<div class="container">
		<div class="row text-center intro">
			<div class="col-12">
				<h2>Fun Facts</h2>
				<p class="text-max-800">Over the years we have done many things that we are proud of. This motivates us to continue looking for new challenges in order to improve our services.</p>
			</div>
		</div>
		<div data-aos-id="counter" data-aos="fade-up" data-aos-delay="200" class="row justify-content-center text-center items">
			<div class="col-12 col-md-6 col-lg-3 item">
				<div data-percent="34822" class="radial">
					<span></span>
				</div>
				<h4>Szál cigi</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-3 item">
				<div data-percent="13791" class="radial">
					<span></span>
				</div>
				<h4>Kávé</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-3 item">
				<div data-percent="745" class="radial">
					<span></span>
				</div>
				<h4>Letört fog</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-3 item">
				<div data-percent="32231" class="radial">
					<span></span>
				</div>
				<h4>Lorem ipsum</h4>
			</div>
		</div>
	</div>
</section>