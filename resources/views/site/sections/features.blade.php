<section id="features" class="section-2 odd offers featured">
	<div class="container">
		<div class="row justify-content-center text-center items">
			<div class="col-12 col-md-6 col-lg-4 item">
				<div class="card no-hover">
					<i class="icon icon-globe"></i>
					<h4>Website Pro</h4>
					<p>We build professional responsive websites optimized for the most popular search engines.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 item">
				<div class="card no-hover">
					<i class="icon icon-basket"></i>
					<h4>E-Commerce</h4>
					<p>Increase your sales with an incredible online store, full of features and functionality.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 item">
				<div class="card no-hover">
					<i class="icon icon-screen-smartphone"></i>
					<h4>Mobile Apps</h4>
					<p>Follow the global trend and create your revolutionary mobile app built with the best technologies.</p>
				</div>
			</div>
		</div>
	</div>
</section>