<section id="slider" class="hero p-0 odd">
	<div class="swiper-container full-slider animation slider-h-100">
		<div class="swiper-wrapper">
			<!-- Item 1 -->
			<div class="swiper-slide slide-center">

				<img src="/assets/site/images/slider1.png" class="full-image" data-mask="70">

				<div class="slide-content row">
					<div class="col-12 d-flex inner">
						<div class="center align-self-center text-center">
							<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
								{{ settings('slide1_title') }}
							</h1>
							<p data-aos="zoom-out-up" data-aos-delay="800" class="description ml-auto mr-auto">
								{{ settings('slide1_content') }}
							</p>
							<a href="#contact" data-aos="zoom-out-up" data-aos-delay="1200" class="smooth-anchor ml-auto mr-auto mt-5 btn primary-button"><i class="icon-cup"></i>
								Kapcsolatfelvétel
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>