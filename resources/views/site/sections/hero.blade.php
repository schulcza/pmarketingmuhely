<!-- Hero -->
<section id="slider" class="hero p-0" style="background-image: url('/assets/site/images/slider1.png'); background-size: cover;">
	<div class="swiper-container no-slider featured animation slider-h-100" >
		<div class="swiper-wrapper">
			<!-- Item 1 -->
			<div class="swiper-slide slide-center">
				{{-- <img data-aos="zoom-out-up" data-aos-delay="800" src="/assets/site/images/slider1.png" class="hero-image" alt="Hero Image"> --}}
				<div class="slide-content row" data-mask-768="70">
					<div class="col-12 d-flex inner">
						<div class="left align-self-center text-center text-md-left">
							<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
								Figyelünk<br>Rád!
							</h1>
							<p data-aos="zoom-out-up" data-aos-delay="800" class="description">
								Mert az igazi sikerhez rád és ránk is szükségünk van!
							</p>
							<a href="#contact" data-aos="zoom-out-up" data-aos-delay="1200" class="smooth-anchor ml-auto mr-auto ml-md-0 mt-4 btn primary-button">
								<i class="icon-phone"></i>
								KAPCSOLATFELVÉTEL
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="swiper-pagination"></div>
	</div>
</section>