<!-- Services -->
<section id="szolgaltatasaink" class="section-4 offers">
	<div class="container">
		<div class="row intro">
			<div class="col-12 col-md-9 align-self-center text-center text-md-left">
				<h2 class="featured">Szolgáltatásaink</h2>
				<p>Focused on results we seek to raise the level of our customers.</p>
			</div>
			<div class="col-12 col-md-3 align-self-end">
				<a href="#contact" class="smooth-anchor btn mx-auto mr-md-0 ml-md-auto primary-button"><i class="icon-speech"></i>
				KONZULTÁCIÓ</a>
			</div>
		</div>
		<div class="row justify-content-center text-center items">
			@foreach ($services as $service)
				<div class="col-12 col-md-6 item">
					<div class="card featured">
						<i class="icon {{ $service->icon }}"></i>
						<h4>
							{{ $service->name }}
						</h4>
						
						<p>{{ $service->excerpt }}</p>
						<a href="{{ route('services.show', $service->slug) }}"><i class="btn-icon icon-arrow-right-circle"></i></a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>