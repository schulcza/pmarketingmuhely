<section id="portfolio" class="section-3 odd offers secondary featured left">
	<div class="container">
		<div class="row intro">
			<div class="col-12 col-md-9 align-self-center text-center text-md-left">
				<h2 class="featured">Legújabb munkáink</h2>
				<p>We have already built amazing things for our customers.</p>
			</div>
			<div class="col-12 col-md-3 align-self-end">
				<a href="{{ route('projects.index') }}" class="btn mx-auto mr-md-0 ml-md-auto primary-button">
					<i class="icon-grid"></i>
					ÖSSZES MUNKÁNK
				</a>
			</div>
		</div>
		<div class="row justify-content-center text-center items">
			@foreach ($projects as $project)
				<div class="col-12 col-md-6 col-lg-4 item">
					<a href="{{ route('projects.show', $project->slug) }}">
						<div class="card featured">
							<h4>
								{{ $project->title }}
							</h4>
							<div class="gallery">
								<img src="{{ $project->getImage('thumbnail') }}" alt="{{ $project->title }}" class="project-image">
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</section>