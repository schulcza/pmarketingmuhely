@extends('layouts.site')

@section('content')
	<!-- Hero -->
	<section id="slider" class="hero p-0 odd featured">
		<div class="swiper-container no-slider slider-h-60">
			<div class="swiper-wrapper">

				<!-- Item 1 -->
				<div class="swiper-slide slide-center">
					
					<img src="{{ $project->getImage('normal') }}" class="full-image" data-mask="70">
					
					<div class="slide-content row text-center">
						<div class="col-12 mx-auto inner">
							{{-- <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
								{{ $project->title }}
							</h1> --}}
							{{-- <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/#demos">Home</a></li>
									<li class="breadcrumb-item"><a href="/#pages">Pages</a></li>
									<li class="breadcrumb-item active" aria-current="page">Single Portfolio</li>
								</ol>
							</nav> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- About [image] -->
	<section id="about" class="section-1 highlights image-right">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 text-center text-md-left">
					<div class="row intro mb-0">
						<div class="col-12 p-0">
							<h3 class="featured">
								{{ $project->title }}
							</h3>
							<p>{!! $project->body !!}</p>
						</div>
					</div>
				</div>
				<div class="gallery col-12 col-md-6">
					<a href="{{ $project->getImage('normal', 'image') }}">
						<img src="{{ $project->getImage('normal', 'image') }}" class="fit-image" alt="{{ $project->title }}">
					</a>
				</div>
			</div>
		</div>
	</section>
@endsection
