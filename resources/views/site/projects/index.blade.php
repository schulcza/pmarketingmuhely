@extends('layouts.site')

@section('content')
<section id="slider" class="hero p-0 odd featured">
	<div class="swiper-container no-slider slider-h-50">
		<div class="swiper-wrapper">

			<!-- Item 1 -->
			<div class="swiper-slide slide-center">
				
				<img src="/assets/site/images/bg-2.jpg" class="full-image" data-mask="70">
				
				<div class="slide-content row text-center">
					<div class="col-12 mx-auto inner">
						<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
							Referenciáink
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="portfolio-grid" class="section-1 showcase portfolio blog-grid filter-section">
	<div class="overflow-holder">
		<div class="container">
			<div class="row justify-content-center text-center">
				<div class="col-12">
					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn active">
							<input type="radio" value="all" checked class="btn-filter-item">
							<span>Mind</span>
						</label>
						@foreach ($projectCategories as $projectCategory)
							<label class="btn">
								<input type="radio" value="{{ $projectCategory->slug }}" class="btn-filter-item">
								<span>
									{{ $projectCategory->name }}
								</span>
							</label>								
						@endforeach
					</div>
				</div>
			</div>                  
			<div class="row items filter-items">
				@foreach ($projects as $project)
					<div class="col-12 col-md-6 col-lg-4 item filter-item" data-groups='{{ $project->projectCategoriesArray }}'>
						<div class="row card p-0 text-center">
							<div class="gallery">
								<a href="{{ route('projects.show', $project->slug) }}" class="image-over">
									<img src="{{ $project->getImage('thumbnail') }}" alt="Lorem ipsum">
								</a>
							</div>
							<div class="card-caption col-12 p-0">
								<div class="card-body">
									<h4 class="m-0">
										{{ $project->title }}
									</h4>
								</div>
								{{-- <div class="card-footer d-lg-flex align-items-center justify-content-center">
									<p>
										{{ $project->body }}
									</p>
								</div> --}}
							</div>
						</div> 
					</div>
				@endforeach
			
				<div class="col-1 filter-sizer"></div>
			</div>
		</div>
	</div>
</section>
@endsection