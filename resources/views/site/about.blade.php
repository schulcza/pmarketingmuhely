@extends('layouts.site')

@section('content')
<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
	<div class="swiper-container no-slider slider-h-50">
		<div class="swiper-wrapper">
			<!-- Item 1 -->
			<div class="swiper-slide slide-center">
				<img src="/assets/site/images/bg-2.jpg" class="full-image" data-mask="70">
				<div class="slide-content row text-center">
					<div class="col-12 mx-auto inner">
						<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
							Rólunk
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Content -->
<section id="content" class="section-1 single">
	<div class="container">
		<div class="row">

			<!-- Main -->
			<main class="col-12 col-lg-8 p-0">
				<div class="row">
					<div class="col-12 align-self-center">
						<h2 class="featured mt-0 ml-0">Content</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum velit tortor, interdum sed cursus eu, sagittis ut nunc. Sed vitae tellus et arcu aliquet faucibus fermentum non lacus.</p>
						<p>Praesent fringilla quis massa et placerat. Mauris eu dui eget urna pellentesque gravida vitae quis nibh. Ut at augue tortor. Pellentesque quis suscipit magna.</p>
						<p>
							<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam tortor, ultrices accumsan mauris eget, pulvinar tincidunt erat. Sed nisi nisi, rutrum sit amet elit.</blockquote>
						</p>
						<p>Sed mauris nulla, tempor eu est vel, dapibus hendrerit mauris curabitur dictum pharetra.</p>
						<p>Etiam mollis sem sed bibendum blandit. Aenean quis luctus ligula, vitae suscipit dui. Nunc diam orci, tincidunt eget consectetur sit amet, vulputate.</p>
						<h4>Lorem ipsum dolor</h4>
						<ul>
							<li>Lorem ipsum dolor sit amet.</li>
							<li>Consectetur adipiscing elit.</li>
							<li>Integer molestie lorem at massa.</li>
							<li>Facilisis in pretium nisl aliquet.</li>
						</ul>
						<p>Sed mauris nulla, tempor eu est vel, dapibus hendrerit mauris curabitur dictum pharetra.</p>
						<p>Etiam mollis sem sed bibendum blandit. Aenean quis luctus ligula, vitae suscipit dui. Nunc diam orci, tincidunt eget consectetur sit amet, vulputate.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum velit tortor, interdum sed cursus eu, sagittis ut nunc. Sed vitae tellus et arcu aliquet faucibus fermentum non eu dui eget urna pellentesque gravida vitae quis.</p>
					</div>
				</div>        
			</main>

			<!-- Sidebar -->
			<aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">                    
				<div class="row">
					<div class="col-12 align-self-center text-left">

						<!-- Widget [services] -->
						<div class="item widget-services">
							<ul class="list-group list-group-flush">
								<li class="list-group-item">
									<a href="#">
										<i class="icon icon-badge"></i>
										<h5>Quality</h5>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<i class="icon icon-briefcase"></i>
										<h5>Experience</h5>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<i class="icon icon-trophy"></i>
										<h5>Tradition</h5>
									</a>
								</li>
								<li class="list-group-item">
									<a href="#">
										<i class="icon icon-diamond"></i>
										<h5>Innovation</h5>
									</a>
								</li>
							</ul>
						</div>

						<!-- Widget [testimonials] -->
						<div class="item widget-carousel">
							<div class="swiper-container mid-slider-simple items" data-perview="1">
								<div class="swiper-wrapper">
									<div class="swiper-slide slide-center text-center item">
										<div class="row card">
											<div class="col-12">
												<img src="/assets/images/image_placeholder.png" alt="Adams Baker" class="person">
												<h4>Adams Baker</h4>
												<p>My website looks amazing with the Leverage Theme.</p>
												<ul class="navbar-nav social share-list ml-auto">
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="swiper-slide slide-center text-center item">
										<div class="row card">
											<div class="col-12">
												<img src="/assets/images/image_placeholder.png" alt="Mary Evans" class="person">
												<h4>Mary Evans</h4>
												<p>This company created an exclusive form. Fantastic.</p>
												<ul class="navbar-nav social share-list ml-auto">
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="swiper-slide slide-center text-center item">
										<div class="row card">
											<div class="col-12">
												<img src="/assets/images/image_placeholder.png" alt="Sarah Lopez" class="person">
												<h4>Sarah Lopez</h4>
												<p>I'm loving the partnership. The support deserves 5 stars.</p>
												<ul class="navbar-nav social share-list ml-auto">
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="swiper-slide slide-center text-center item">
										<div class="row card">
											<div class="col-12">
												<img src="assets/images/team-4.jpg" alt="Joseph Hills" class="person">
												<h4>Joseph Hills</h4>
												<p>My app was perfect. I will request more services soon.</p>
												<ul class="navbar-nav social share-list ml-auto">
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="swiper-slide slide-center text-center item">
										<div class="row card">
											<div class="col-12">
												<img src="assets/images/team-5.jpg" alt="Karen Usman" class="person">
												<h4>Karen Usman</h4>
												<p>I had small problems with the payment, but it was resolved.</p>
												<ul class="navbar-nav social share-list ml-auto">
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
													<li class="nav-item">
														<a href="#" class="nav-link"><i class="icon-star ml-2 mr-2"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-pagination"></div>
							</div>
						</div>
					</div>
				</div>
			</aside>
		</div>
	</div>
</section>

 <!-- Team -->
 <section id="team" class="section-4 carousel featured card-white">
	<div class="overflow-holder">
		<div class="container">
			<div class="row text-center intro">
				<div class="col-12">
					<h2 class="super effect-static-text">Team of Experts</h2>
					<p class="text-max-800">Our team will help you define a stand-out creative direction and will translate it into visual assets that will resonate with your audience.</p>
				</div>
			</div>
			<div class="swiper-container mid-slider-simple items">
				<div class="swiper-wrapper">
					<div class="swiper-slide slide-center text-center item">
						<div class="row card">
							<div class="col-12">
								<img src="/assets/images/image_placeholder.png" alt="Adams Baker" class="person">
								<h4>Adams Baker</h4>
								<p>CEO / CO-FOUNDER</p>
								<ul class="navbar-nav social share-list ml-auto">
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-instagram"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="swiper-slide slide-center text-center item">
						<div class="row card">
							<div class="col-12">
								<img src="/assets/images/image_placeholder.png" alt="Mary Evans" class="person">
								<h4>Mary Evans</h4>
								<p>CONTROLLING / FOUNDER</p>
								<ul class="navbar-nav social share-list ml-auto">
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-instagram"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="swiper-slide slide-center text-center item">
						<div class="row card">
							<div class="col-12">
								<img src="/assets/images/image_placeholder.png" alt="Sarah Lopez" class="person">
								<h4>Sarah Lopez</h4>
								<p>HUMAN RESOURCES</p>
								<ul class="navbar-nav social share-list ml-auto">
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-instagram"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
	</div>
</section>

@endsection