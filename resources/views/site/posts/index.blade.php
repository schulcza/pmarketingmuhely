@extends('layouts.site')

@section('content')
<section id="slider" class="hero p-0 odd featured">
	<div class="swiper-container no-slider slider-h-50">
		<div class="swiper-wrapper">

			<!-- Item 1 -->
			<div class="swiper-slide slide-center">
				
				<img src="/assets/site/images/bg-2.jpg" class="full-image" data-mask="70">
				
				<div class="slide-content row text-center">
					<div class="col-12 mx-auto inner">
						<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
							Blog
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="blog" class="section-1 showcase blog-grid masonry news">
	<div data-aos="zoom-in" data-aos-delay="200" data-aos-anchor="body" class="container">
		<div class="row content blog-grid masonry">
			<main class="col-12 p-0">
				<div class="bricklayer items columns-3">
					@foreach ($posts as $post)
						<div class="card p-0 text-center item">
							<div class="image-over">
								<img src="{{ $post->getImage('normal') }}" alt="{{ $post->title }}">
							</div>
							<div class="card-caption col-12 p-0">
								<div class="card-body">
									<a href="{{ route('posts.show', $post->slug) }}">
										<h4>{{ $post->title }}</h4>
									</a>
								</div>
								<div class="card-footer d-lg-flex align-items-center justify-content-center">
									{{-- <a href="#" class="d-lg-flex align-items-center"><i class="icon-user"></i>John Doe</a> --}}
									<a href="#" class="d-lg-flex align-items-center"><i class="icon-clock"></i>
										{{ dateFormat($post->created_at) }}
									</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</main>
		</div>

		<!-- Pagination -->
		<div class="row">
			<div class="col-12">
				<nav>
					<ul class="pagination justify-content-center">
						<li class="page-item">
							<a class="page-link" href="#" tabindex="-1">
								<i class="icon-arrow-left"></i>
							</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item active"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item">
							<a class="page-link" href="#">
								<i class="icon-arrow-right"></i>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
@endsection