@extends('layouts.site')

@section('content')
	<!-- Hero -->
	<section id="slider" class="hero p-0 odd featured">
		<div class="swiper-container no-slider slider-h-60">
			<div class="swiper-wrapper">

				<!-- Item 1 -->
				<div class="swiper-slide slide-center">
					
					<img src="{{ $post->getImage('normal') }}" class="full-image" data-mask="70">
					
					<div class="slide-content row text-center">
						<div class="col-12 mx-auto inner">
							<h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">
								{{-- {{ $post->title }} --}}
							</h1>
							{{-- <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/#demos">Home</a></li>
									<li class="breadcrumb-item"><a href="/#pages">Pages</a></li>
									<li class="breadcrumb-item active" aria-current="page">Single Portfolio</li>
								</ol>
							</nav> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="blog" class="section-1 single featured">
		<div class="container">
			<div class="row">

				<!-- Main -->
				<main class="col-12 col-lg-8 p-0">
					<div class="row">
						<div class="col-12 align-self-center">
							<h3 class="featured mt-0 ml-0">
								{{ $post->title }}
							</h3>
							{{ $post->excerpt }}
							<hr>
							{!! $post->body !!}
						</div>
					</div>        
				</main>

				<!-- Sidebar -->
				<aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">                    
					<div class="row">
						<div class="col-12 align-self-center text-left">                                
							<!-- Widget [author] -->
							<div class="item widget-author">  
								<ul class="list-group list-group-flush">
									<li class="list-group-item d-flex justify-content-between align-items-center">
										<span class="d-lg-flex align-items-center">
											<i class="icon-clock mr-2"></i>
											{{ dateTimeFormat($post->created_at) }}
										</span>
									</li>
									<li class="list-group-item d-flex">
										@foreach ($post->postCategories as $postCategory)
											<span class="badge tag m-0 active">
												{{ $postCategory->name }}
											</span>
										@endforeach
									</li>
								</ul>                                      
							</div>

							<!-- Widget [categories] -->
							<div class="item widget-categories">  
								<h4 class="title">Categories</h4>
								<ul class="list-group list-group-flush">
									@foreach ($postCategories as $postCategory)
										<li class="list-group-item d-flex justify-content-between align-items-center">
											<a href="#">
												{{ $postCategory->name }}
											</a>
											{{-- <span class="badge circle">23</span> --}}
										</li>
									@endforeach
								</ul>                                
							</div>

							<!-- Widget [share-this] -->
							<div class="item widget-share-this">
								<h4 class="title">Share This</h4>
								<ul class="navbar-nav social share-list">
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-instagram ml-0"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
									</li>
									<li class="nav-item">
										<a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</section>
@endsection
