<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>{{ env('APP_NAME') }}</title>
		<meta name="description" content="Ipari ingatlan bejelentkezés" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Styles-->
		<link href="/assets/dashboard/css/auth.css" rel="stylesheet" type="text/css" />
		<!--begin::Styles-->
		<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			@yield('content')
		</div>
		<!--end::Main-->
	</body>
	<!--end::Body-->
</html>