<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ settings('meta_title') }} | @yield('title', $page_title ?? '')</title>

    <!-- Fonts -->
	{{ Metronic::getGoogleFontsInclude() }}
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

	{{-- Global Theme Styles (used by all pages) --}}
	@foreach(config('layout.resources.css') as $style)
		<link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet" type="text/css"/>
	@endforeach
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.css">

	<meta name="_token" content="{{ csrf_token() }}">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <!-- Icons -->
    {{-- <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet"> --}}
    
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js" integrity="sha512-G3jAqT2eM4MMkLMyQR5YBhvN5/Da3IG6kqgYqU9zlIH4+2a+GuMdLb5Kpxy6ItMdCfgaKlo2XFhI0dHtMJjoRw==" crossorigin="anonymous"></script> --}}


	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>


    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
</head>
<body {{ Metronic::printAttrs('body') }} class="{{ Metronic::printClasses('body', false) }} @hasSection('page_toolbar') subheader-enabled @endif">

	@if (config('layout.page-loader.type') != '')
		@include('app.partials._page-loader')
	@endif

	@include('app.base._layout')

	<script>
		var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
	</script>

	{{-- Global Theme JS Bundle (used by all pages)  --}}
	@foreach(config('layout.resources.js') as $script)
		<script src="{{ asset($script) }}" type="text/javascript"></script>
	@endforeach

	<script src="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.js"></script>

	{{-- Includable JS --}}
	@stack('scripts') 

	{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.10/vue.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js"></script>
    <script src="/js/vue-app.js"></script> --}}

    @if (session()->has('success')) 
		<script>
			const notyf = new Notyf({dismissible: true})
			notyf.success('{{ session('success') }}')
		</script> 
    @endif

    <script>
        /* Simple function to retrieve data url from file */
        function fileToDataUrl(event, callback) {
            if (! event.target.files.length) return
                    
            let file = event.target.files[0], 
                reader = new FileReader()
            
            reader.readAsDataURL(file)
            reader.onload = e => callback(e.target.result)
        }

		$('.summernote').summernote({
			height: 250
		});

		$('.multiselect').select2({
			placeholder: "Select value(s)",
		});
    </script>
</body>
</html>