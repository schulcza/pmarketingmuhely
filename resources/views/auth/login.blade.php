{{-- Extends layout --}}
@extends('layouts.auth')

{{-- Content --}}
@section('content')

<!--begin::Login-->
<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
	<!--begin::Aside-->
	<div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
		<!--begin::Aside Bottom-->
		<div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(https://picsum.photos/600/800)"></div>
		<!--end::Aside Bottom-->
	</div>
	<!--begin::Aside-->
	<!--begin::Content-->
	
	<div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
		<!--begin::Content body-->
		<div class="d-flex flex-column-fluid flex-center">
			<!--begin::Signin-->
			<div class="login-form login-signin">
				<!--begin::Form-->
				<form class="form" novalidate="novalidate" method="POST" action="{{ route('login') }}">
					@csrf
					<!--begin::Title-->
					<div class="pb-13 pt-lg-0 pt-5">
						<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">{{ __('auth.login_title') }}</h3>
					</div>
					<!--begin::Title-->
					<!--begin::Form group-->
					<div class="form-group">
						<label class="font-size-h6 font-weight-bolder text-dark">{{ __('auth.email') }}</label>
						<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="text" name="email" autocomplete="off" value="{{ old('email') }}" />
					</div>
					<!--end::Form group-->
					<!--begin::Form group-->
					<div class="form-group">
						<div class="d-flex justify-content-between mt-n5">
							<label class="font-size-h6 font-weight-bolder text-dark pt-5">{{ __('auth.password') }}</label>
							<a href="{{ route('password.request') }}" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" >{{ __('auth.forgot_password') }}</a>
						</div>
						<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="password" name="password" autocomplete="off" />
					</div>
					<!--end::Form group-->
					<span class="error" >
						@foreach($errors->all() as $error)
							<strong>{{ $error }}</strong>
						@endforeach
					</span>
					<!--begin::Action-->
					<div class="pb-lg-0 pb-5">
						<button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">{{ __('auth.login') }}</button>
					</div>
					<!--end::Action-->
				</form>
				<!--end::Form-->
			</div>
			<!--end::Signin-->
		</div>
		<!--end::Content body-->
	</div>
	<!--end::Content-->
</div>
<!--end::Login-->
@endsection