<div class="row">
	<x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="title"
            label="{{ __('projects.title') }}"
            value="{{ old('title', ($editing ? $project->title : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

	<x-inputs.group class="col-sm-12">
		<x-inputs.basic
			type="url"
			name="url"
			label="{{ __('projects.url') }}"
			value="{{ old('url', ($editing ? $project->url : '')) }}"
			maxlength="255"
			required
		></x-inputs.basic>
    </x-inputs.group>

	<x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="body"
            label="{{ __('projects.body') }}"
            required
			class="summernote"
			rows="10"
        >{{ old('body', ($editing ? $project->body : '')) }}</x-inputs.textarea>
	</x-inputs.group>
</div>
