@extends('layouts.app')

@section('content')
{{-- Delete modal --}}
@include('app.partials.delete-modal')

<div class="card card-custom gutter-b" id="users">
	<div class="card-body">
		<div class="searchbar mt-4 mb-5">
			<div class="row">
				<div class="col-md-8">
					<form>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<input
										id="search"
										type="text"
										name="search"
										placeholder="{{ __('common.search') }}"
										value="{{ $search ?? '' }}"
										class="form-control"
										autocomplete="off"
									/>
									<div class="input-group-append">
										<button
											type="submit"
											class="btn btn-primary"
										>
											{{ Metronic::getSVG('assets/images/svg/icons/General/Search.svg', 'svg-icon svg-icon-md') }}
										</button>
									</div>
									
								</div>
							</div>
							<div class="col-md-2">
								<a
									href="{{ route('dashboard.posts.index') }}"
									class="btn btn-primary ml-2"
								>
									{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Close.svg', 'svg-icon svg-icon-md') }}
								</a>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4 text-right">
					<a
						href="{{ route('dashboard.posts.create') }}"
						class="btn btn-primary"
					>
						{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Plus.svg', 'svg-icon svg-icon-md') }}
						@lang('common.create')
					</a>

				</div>
			</div>
		</div>

		<div class="table-responsive">
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>
							@sortablelink('title', __('posts.title'))
						</th>
						<th>
							{{ __('posts.featured') }}
						</th>
						<th>
							{{ __('posts.categories') }}
						</th>
						<th>
							@sortablelink('created', __('common.created_at'))
						</th>	
						<th class="text-center">
							@lang('common.actions')
						</th>
					</tr>
				</thead>
				<tbody>
					@forelse($posts as $post)
						<tr>
							<td>{{ $post->title ?? '-' }}</td>
							<td>
								{{ Metronic::getSVG("assets/images/svg/icons/Navigation/" . ($post->featured ? "Check" : "Minus") . ".svg", "svg-icon-xl") }}
							</td>
							<td>
								@foreach ($post->postCategories as $postCategory)
									{{ $postCategory->name }}
									@if (!$loop->last),@endif
								@endforeach
							</td>
							<td>{{ dateTimeFormat($post->created_at) }}</td>
							<td class="text-center" style="width: 134px;">
								<div
									role="group"
									aria-label="Row Actions"
									class="btn-group"
								>
									<a aria-label="Edit" data-balloon-pos="up"  href="{{ route('dashboard.posts.edit', $post) }}" class="btn btn-sm btn-light-warning btn-text-primary btn-hover-primary btn-icon mr-2" title="">
										{{ Metronic::getSVG('assets/images/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md') }}
									</a>

									<a aria-label="Delete" data-balloon-pos="up" onclick="showDeleteModal('{{ $post->id }}', '{{ \Request::getPathInfo() }}/')" data-toggle="modal" data-target="#delete-modal" data-placement="top" href="" class="btn btn-sm btn-light-danger btn-text-primary btn-hover-primary btn-icon mr-2" title="">
										{{ Metronic::getSVG('assets/images/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md') }}
									</a>										
								</div>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="8">
								@lang('common.no_items_found')
							</td>
						</tr>
					@endforelse
				</tbody>
			</table>
			{{ $posts->appends(request()->query())->links('app.partials.pagination') }}
		</div>
	</div>
</div>
@endsection