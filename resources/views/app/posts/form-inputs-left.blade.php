<div class="row">
	<x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="title"
            label="{{ __('posts.title') }}"
            value="{{ old('title', ($editing ? $post->title : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

	<x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="excerpt"
            label="{{ __('posts.excerpt') }}"
            required
			rows="10"
        >{{ old('excerpt', ($editing ? $post->excerpt : '')) }}</x-inputs.textarea>
	</x-inputs.group>

	<x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="body"
            label="{{ __('posts.body') }}"
            required
			class="summernote"
			rows="10"
        >{{ old('body', ($editing ? $post->body : '')) }}</x-inputs.textarea>
	</x-inputs.group>
</div>
