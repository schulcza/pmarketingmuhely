@php $editing = isset($post) @endphp

{{-- Extends layout --}}
@extends('layouts.app')

{{-- Content --}}
@section('content')

<!--begin::Form-->
<x-form
	method="POST"
	action="{{ route('dashboard.posts.store', $post ?? null) }}"
	class="mt-4"
	enctype="multipart/form-data"
>
	<div class="row justify-content-center">
		<div class="col-xl-9">
			<div class="card card-custom gutter-b">
				<!--begin::Card body-->
				<div class="card-body">
					@include('app.posts.form-inputs-left')
				</div>
				<!--end::Card body-->
			</div>
		</div>
		<div class="col-xl-3">
			<div class="card card-custom gutter-b">
				<div class="card-body">
					<div class="justify-content-center">
						@include('app.posts.form-inputs-right')
					</div>
				</div>
				<!--begin::Card footer-->
				<div class="card-footer">
					<div class="row">
						<div class="col-lg-12">
							<a href="{{ route('dashboard.posts.index') }}" class="btn btn-secondary">
								{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Arrow-left.svg', 'svg-icon svg-icon-md') }}
								@lang('common.back')
							</a>
							<button type="submit" class="btn btn-primary float-right">
								{{ Metronic::getSVG('assets/images/svg/icons/General/Save.svg', 'svg-icon svg-icon-md') }}
								@lang('common.' . ($editing ? 'update' : 'save'))
							</button>
						</div>
					</div>
				</div>
				<!--end::Card footer-->
			</div>
		</div>
	</div>
</x-form>
<!--end::Form-->

@endsection