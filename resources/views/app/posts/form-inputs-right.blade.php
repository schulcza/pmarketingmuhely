<div class="row">
	<x-inputs.group class="col-sm-12">
		<label class="label">Featured Image</label>
		<div class="image-input image-input-outline" id="featured_image">
			<div class="image-input-wrapper" style="background-image: url({{ $editing ? $post->getImage('thumbnail', 'featured_image') : '' }})"></div>
			<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="{{ __('common.change') }}">
				<i class="fa fa-upload icon-sm text-muted"></i>
				<input type="file" name="featured_image" accept=".png, .jpg, .jpeg" />
				<input type="hidden" name="featured_image_remove" />
			</label>
			<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="{{ __('common.cancel') }}">
				<i class="ki ki-bold-close icon-xs text-muted"></i>
			</span>
			<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="{{ __('common.delete') }}">
				<i class="ki ki-bold-close icon-xs text-muted"></i>
			</span>
		</div>
	</x-inputs.group>

	<x-inputs.group class="col-sm-12">
        <x-inputs.select name="featured" label="{{ __('posts.featured') }}" required>
            @php $selected = old('featured', ($editing ? $post->featured : '')) @endphp
			<option value="1" {{ $selected == true ? 'selected' : '' }} >
				YES
			</option>
			<option value="0" {{ $selected == false ? 'selected' : '' }} >
				NO
			</option>
        </x-inputs.select>
    </x-inputs.group>

	<x-inputs.group class="col-sm-12">
        <x-inputs.select name="categories[]" label="{{ __('posts.categories') }}" multiple="multiple" class="multiselect" required>
            {{-- @php $selected = old('categories', ($editing ? $post->featured : '')) @endphp --}}
			@foreach ($postCategories as $postCategory)
				<option value="{{ $postCategory->id }}" {{ $editing ? (in_array($postCategory->id, $post->categories) ? 'selected' : '') : '' }}>
					{{ $postCategory->name }}
				</option>
			@endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>