<div class="row">
	<x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="{{ __('project-categories.name') }}"
            value="{{ old('name', ($editing ? $projectCategory->name : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
