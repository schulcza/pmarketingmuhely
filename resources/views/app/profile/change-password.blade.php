{{-- Extends layout --}}
@extends('layouts.app')

{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container container-full">
		<!--begin::Profile Personal Information-->
		<div class="d-flex flex-row">
			@include('app.profile.partials.sidebar')
			<!--begin::Content-->
			<div class="flex-row-fluid ml-lg-8">
				<!--begin::Card-->
				<!--begin::Form-->
				<form action="{{ route('dashboard.profile.password.update') }}" method="POST" class="form">
					@csrf
					<div class="card card-custom">
						<!--begin::Header-->
						<div class="card-header py-3">
							<div class="card-title align-items-start flex-column">
								<h3 class="card-label font-weight-bolder text-dark">{{ __('auth.change_password') }}</h3>
							</div>
							<div class="card-toolbar">
								<button type="submit" class="btn btn-success mr-2">{{ __('common.save') }}</button>
							</div>
						</div>
						<!--end::Header-->
						<div class="card-body">
							<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label text-alert">{{ __('auth.current_password') }}</label>
								<div class="col-lg-9 col-xl-6">
									<input type="password" class="form-control form-control-lg form-control-solid mb-2" name="old_password" placeholder="{{ __('auth.current_password') }}" />
									<a href="{{ route('password.request') }}" class="text-sm font-weight-bold">{{ __('auth.forgot_password') }}</a>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label text-alert">{{ __('auth.new_password') }}</label>
								<div class="col-lg-9 col-xl-6">
									<input type="password" class="form-control form-control-lg form-control-solid" name="password" placeholder="{{ __('auth.new_password') }}" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label text-alert">{{ __('auth.password_confirm') }}</label>
								<div class="col-lg-9 col-xl-6">
									<input type="password" class="form-control form-control-lg form-control-solid" name="password_confirmation" placeholder="{{ __('auth.password_confirm') }}" />
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Content-->
		</div>
		<!--end::Profile Personal Information-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
