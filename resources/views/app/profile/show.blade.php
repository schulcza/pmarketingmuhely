{{-- Extends layout --}}
@extends('layouts.app')

{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container container-full">
		<!--begin::Profile Personal Information-->
		<div class="d-flex flex-row">
			@include('app.profile.partials.sidebar')
			<!--begin::Content-->
			<div class="flex-row-fluid ml-lg-8">
				<!--begin::Card-->
				<!--begin::Form-->
				<form action="{{ route('dashboard.profile.update') }}" method="POST" class="form">
					@csrf
					<div class="card card-custom card-stretch">
						<!--begin::Header-->
						<div class="card-header py-3">
							<div class="card-title align-items-start flex-column">
								<h3 class="card-label font-weight-bolder text-dark">{{ __('users.base_data') }}</h3>
							</div>
							<div class="card-toolbar">
								<button type="submit" class="btn btn-success mr-2">{{ __('common.save') }}</button>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body">
							<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label text-right">{{ __('users.name') }}</label>
								<div class="col-lg-9 col-xl-6">
									<input class="form-control form-control-lg form-control-solid" type="text" name="name" value="{{ old('name') ?? $user->name }}" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label text-right">{{ __('users.email') }}</label>
								<div class="col-lg-9 col-xl-6">
									<input class="form-control form-control-lg form-control-solid" type="text" name="email" value="{{ old('email') ?? $user->email }}" />
								</div>
							</div>
						</div>
						<!--end::Body-->
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Content-->
		</div>
		<!--end::Profile Personal Information-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
