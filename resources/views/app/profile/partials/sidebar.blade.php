<!--begin::Aside-->
<div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
	<!--begin::Profile Card-->
	<div class="card card-custom card-stretch">
		<!--begin::Body-->
		<div class="card-body pt-4">
			<!--begin::User-->
			<div class="d-flex align-items-center">
				<div class="symbol symbol-60 symbol-xl-50 mr-5 align-self-start align-self-xxl-center">
					<div class="symbol symbol-50 symbol-lg-50 symbol-primary">
						<span class="font-size-h3 symbol-label font-weight-boldest">{{ substr($user->name, 0, 1) }}</span>
					</div>
				</div>
				<div>
					<a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{ $user->name }}</a>
					<div class="text-muted">
						{{ $user->email }}
					</div>
				</div>
			</div>
			<!--end::User-->
			<!--begin::Nav-->
			<div class="navi navi-bold navi-hover navi-active navi-link-rounded mt-10">
				<div class="navi-item mb-2">
					<a href="{{ route('dashboard.profile.show') }}" class="navi-link py-4 {{ request()->url() == route('dashboard.profile.show') ? 'active' : '' }}">
						{{ Metronic::getSVG("assets/admin/images/svg/icons/General/User.svg", "mr-2") }}
						<span class="navi-text font-size-lg">{{ __('users.base_data') }}</span>
					</a>
				</div>
				<div class="navi-item mb-2">
					<a href="{{ route('dashboard.profile.password.show') }}" class="navi-link py-4 {{ request()->url() == route('dashboard.profile.password.show') ? 'active' : '' }}">
						{{ Metronic::getSVG("assets/admin/images/svg/icons/Communication/Shield-user.svg", "mr-2") }}
						<span class="navi-text font-size-lg">{{ __('users.change_password') }}</span>
					</a>
				</div>
			</div>
			<!--end::Nav-->
		</div>
		<!--end::Body-->
	</div>
	<!--end::Profile Card-->
</div>
<!--end::Aside-->