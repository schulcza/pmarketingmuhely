<div class="row">
	<x-inputs.group class="col-sm-8">
        <x-inputs.text
            name="name"
            label="{{ __('services.name') }}"
            value="{{ old('name', ($editing ? $service->name : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

	<x-inputs.group class="col-sm-4">
        <x-inputs.select name="icon" label="{{ __('services.icon') }}" class="selectpicker" data-live-search="true" required>
            @php $selected = old('icon', ($editing ? $service->icon : '')) @endphp
			@foreach (config('icons') as $icon)
				<option data-icon="la {{ $icon['value'] }} font-size-lg bs-icon" {{ $selected == $icon['value'] ? 'selected' : '' }} value="{{ $icon['value'] }}	">
					{{ $icon['name'] }}	
				</option>				
			@endforeach
        </x-inputs.select>
    </x-inputs.group>

	<x-inputs.group class="col-sm-10">
        <x-inputs.textarea
            name="excerpt"
            label="{{ __('services.excerpt') }}"
			rows="5"
        >{{ old('excerpt', ($editing ? $service->excerpt : '')) }}</x-inputs.textarea>
	</x-inputs.group>

	<x-inputs.group class="col-sm-2">
		<label class="label">Image</label>
		<div class="image-input image-input-outline" id="image">
			<div class="image-input-wrapper" style="background-image: url({{ $editing ? $service->getImage('thumbnail', 'image') : '' }})"></div>
			<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="{{ __('common.change') }}">
				<i class="fa fa-upload icon-sm text-muted"></i>
				<input type="file" name="image" accept=".png, .jpg, .jpeg" />
				<input type="hidden" name="image_remove" />
			</label>
			<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="{{ __('common.cancel') }}">
				<i class="ki ki-bold-close icon-xs text-muted"></i>
			</span>
			<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="{{ __('common.delete') }}">
				<i class="ki ki-bold-close icon-xs text-muted"></i>
			</span>
		</div>
	</x-inputs.group>
	
	<x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="body"
            label="{{ __('services.body') }}"
			class="summernote"
			rows="10"
        >{{ old('body', ($editing ? $service->body : '')) }}</x-inputs.textarea>
	</x-inputs.group>
</div>
