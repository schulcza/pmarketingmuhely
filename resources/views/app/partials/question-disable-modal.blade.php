<div class="modal fade" id="question-disable-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="post">
				@csrf
				<input type="hidden" name="id" value="">
				<input type="hidden" name="method" value="post">
			
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Are you sure to disable question?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{ __('common.cancel') }}</button>
					<button type="submit" class="btn btn-primary font-weight-bold">{{ __('common.yes') }}</button>
				</div>
			</form>
		</div>
	</div>
</div>