{{-- Header --}}
<div class="d-flex align-items-center justify-content-between flex-wrap p-8 bgi-size-cover bgi-no-repeat rounded-top">
	<div class="d-flex align-items-center mr-2">
		{{-- Symbol --}}
		<div class="symbol bg-white-o-15 mr-3">
			<span class="symbol-label text-success font-weight-bold font-size-h4">{{ substr(\Auth::user()->name, 0, 1) }}</span>
		</div>

		{{-- Text --}}
		<div class="m-0 flex-grow-1 mr-3 font-size-h5">{{ \Auth::user()->name }}
			<div class="text-muted">
				{{ \Auth::user()->email }}
			</div>
		</div>
	</div>
</div>

{{-- Nav --}}
<div class="navi navi-spacer-x-0 pt-5">
    {{-- Item --}}
    <a href="{{ route('dashboard.profile.show') }}" class="navi-item px-8">
        <div class="navi-link">
            <div class="navi-icon mr-2">
                <i class="flaticon2-calendar-3 text-success"></i>
            </div>
            <div class="navi-text">
                <div class="font-weight-bold">
                    {{ __('common.my_profile') }}
                </div>
                <div class="text-muted">
                    {{ __('common.my_profile_desc') }}
                </div>
            </div>
        </div>
    </a>

    {{-- Footer --}}
    <div class="navi-separator mt-3"></div>
    <div class="navi-footer  px-8 py-5">
		{{-- <a href="#" target="_blank" class="btn btn-light-primary font-weight-bold">Kijelentkezés</a> --}}
		<a class="btn btn-light-primary font-weight-bold" href="{{ route('logout') }}"
			onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
			{{ __('common.logout') }}
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			@csrf
		</form>
        {{-- <a href="#" target="_blank" class="btn btn-clean font-weight-bold">Upgrade Plan</a> --}}
    </div>
</div>
