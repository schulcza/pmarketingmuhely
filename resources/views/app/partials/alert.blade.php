@php
	$alert_type = null;

	// if(Session::has('success')) {
	// 	$alert_type = 'success';
	// 	$alert_class = 'success';
	// 	$alert_icon = 'flaticon2-check-mark';
	// } elseif (Session::has('error')) {
	// 	$alert_type = 'error';
	// 	$alert_class = 'danger';
	// 	$alert_icon = 'flaticon-danger';
	// } elseif (Session::has('info')) {
	// 	$alert_type = 'info';
	// 	$alert_class = 'info';
	// 	$alert_icon = 'flaticon2-information';
	// }

	if($errors->any()) {
		$alert_type = 'error';
		$alert_class = 'danger';
		$alert_icon = 'flaticon-danger';
	}


@endphp
@if ($alert_type)
	<div class="alert alert-custom alert-notice alert-light-{{ $alert_class }} fade show" role="alert">
		<div class="alert-icon"><i class="{{ $alert_icon }}"></i></div>
		<div class="alert-text">
			<ul>
				@foreach ($errors->all() as $error)
					<li>
						{{ $error }}
					</li>
				@endforeach
			</ul>
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="ki ki-close"></i></span>
			</button>
		</div>
	</div>
@endif