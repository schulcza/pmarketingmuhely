{{-- Subheader V1 --}}
@hasSection('page_toolbar')
{{ Metronic::addClass('body', 'subheader-enabled') }}
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">

		{{-- Info --}}
        <div class="d-flex align-items-center flex-wrap mr-1">

			@yield('page_toolbar')

			{{-- Page Title --}}
            <h5 class="text-dark font-weight-bold my-2 mr-5">
                @if (isset($page_description) && config('layout.subheader.displayDesc'))
                    <small>{{ @$page_description }}</small>
                @endif
			</h5>
        </div>

		{{-- Toolbar --}}
        <div class="d-flex align-items-center">

        </div>

    </div>
</div>
@endif