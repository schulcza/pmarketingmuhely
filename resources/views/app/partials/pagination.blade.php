@if ($paginator->hasPages())
		<!--begin::Pagination-->
		<div class="d-flex justify-content-between align-items-center flex-wrap pull-right">
			<div class="d-flex flex-wrap mr-3">
				{{-- Previous Page Link --}}
				@if ($paginator->onFirstPage())
					<a href="{{ $paginator->previousPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-label="@lang('pagination.previous')">
						<i class="ki ki-bold-arrow-back icon-xs"></i>
					</a>
				@else
					<a href="{{ $paginator->previousPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-label="@lang('pagination.previous')">
						<i class="ki ki-bold-arrow-back icon-xs"></i>
					</a>
				@endif

				{{-- Pagination Elements --}}
				@foreach ($elements as $element)
					{{-- "Three Dots" Separator --}}
					@if (is_string($element))
						<a class="disabled btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 active">...</a>
						{{-- <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li> --}}
					@endif

					{{-- Array Of Links --}}
					@if (is_array($element))
						@foreach ($element as $page => $url)
							@if ($page == $paginator->currentPage())
								<a href="{{ $url }}" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 active">{{ $page }}</a>
								@else
								<a href="{{ $url }}" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">{{ $page }}</a>
							@endif
						@endforeach
					@endif
				@endforeach

				{{-- Next Page Link --}}
				@if ($paginator->hasMorePages())
					<a href="{{ $paginator->nextPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"  aria-label="@lang('pagination.next')">
						<i class="ki ki-bold-arrow-next icon-xs"></i>
					</a>
				@else
					<a disabled href="{{ $paginator->nextPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1"  aria-label="@lang('pagination.next')">
						<i class="ki ki-bold-arrow-next icon-xs"></i>
					</a>
				@endif
			</div>
		</div>
		<!--end:: Pagination-->
<!--end::Pagination-->
@endif
