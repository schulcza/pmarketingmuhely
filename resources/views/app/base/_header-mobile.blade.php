{{-- Header Mobile --}}
<div id="kt_header_mobile" class="header-mobile {{ Metronic::printClasses('header-mobile', false) }}" {{ Metronic::printAttrs('header-mobile') }}>
    <div class="mobile-logo">
        <a href="{{ url('/') }}">
            <img alt="{{ config('app.name') }}" src="{{ asset('assets/dashboard/images/' . config('app.logo_filename')) }}"/>
        </a>
    </div>
    <div class="mobile-toolbar">

        @if (config('layout.aside.self.display'))
            <button class="mobile-toggle mobile-toggle-left" id="kt_aside_mobile_toggle"><span></span></button>
        @endif

        @if (config('layout.header.menu.self.display'))
            <button class="mobile-toggle ml-3" id="kt_header_mobile_toggle"><span></span></button>
        @endif

        <button class="topbar-toggle ml-3" id="kt_header_mobile_topbar_toggle">
            {{ Metronic::getSVG('assets/images/svg/icons/General/User.svg') }}
        </button>
    </div>
</div>
