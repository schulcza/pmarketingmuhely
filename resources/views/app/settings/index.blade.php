{{-- Extends layout --}}
@extends('layouts.app')

{{-- Content --}}
@section('content')
	{{-- Delete modal --}}
	<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="" method="post">
					@csrf
					<input type="hidden" name="_method" value="delete">
					<input type="hidden" name="id" value="">
				
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">{{ __('common.are_you_sure_delete') }}</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i aria-hidden="true" class="ki ki-close"></i>
						</button>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{ __('common.cancel') }}</button>
						<button type="submit" class="btn btn-primary font-weight-bold">{{ __('common.yes_delete') }}</button>
					</div>
				</form>
			</div>
		</div>
	</div>

    <div class="card card-custom gutter-b settings-card">
		<form action="{{ route('dashboard.settings.store') }}" method="POST" class="form settings-index" enctype="multipart/form-data">
			@csrf
			<!--begin::Card body-->
			<div class="card-body">
				@foreach ($setting_groups as $key => $group)
					<h5 class="text-dark font-weight-bold mb-10">{{ $key }}</h5>
					@foreach ($group as $setting)
						<div class="form-group row">
							<label class="col-3 col-form-label">
								{{ $setting->name }}
								@if($setting->help_text)
									<i class="far fa-question-circle" data-toggle="tooltip" data-theme="dark" title="{{ $setting->help_text }}"></i>
								@endif
							</label>
							<div class="col-9">
								@switch($setting->type)
									@case('text')
										<input class="form-control form-control-solid" type="text" name="{{ $setting->keyword }}" value="{{ $setting->value }}"/>
										@break
									@case('number')
										<input class="form-control form-control-solid" type="number" name="{{ $setting->keyword }}" value="{{ $setting->value }}"/>
										@break
									@case('file')
										<div class="image-input image-input-outline" id="{{ $setting->keyword }}">
											<div class="image-input-wrapper" style="background-image: url({{ $setting->getImage('thumbnail') }})"></div>
											<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="{{ __('common.change') }}">
												<i class="fa fa-upload icon-sm text-muted"></i>
												<input type="file" name="{{ $setting->keyword }}" accept=".png, .jpg, .jpeg" />
												<input type="hidden" name="{{ $setting->keyword }}_remove" />
											</label>
											<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="{{ __('common.cancel') }}">
												<i class="ki ki-bold-close icon-xs text-muted"></i>
											</span>
											<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="{{ __('common.delete') }}">
												<i class="ki ki-bold-close icon-xs text-muted"></i>
											</span>
										</div>
										@break
									@case('checkbox')
										<span class="switch">
											<label>
												<input type="checkbox" name="{{ $setting->keyword }}" {{ $setting->value == 'on' ? 'checked' : '' }}/>
												<span></span>
											</label>
										</span>
										@break
									@case('textarea')
										<textarea class="form-control form-control-solid" rows="10" name="{{ $setting->keyword }}">{{ $setting->value }}</textarea>				
										@break
									@default
								@endswitch
							</div>
						</div>
					@endforeach

					@if (!$loop->last)
						<div class="separator separator-solid mb-10"></div>
					@endif
				@endforeach
			</div>
			<!--end::Card body-->
			<!--begin::Card footer-->
			<div class="card-footer">
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-primary mr-2">{{ __('common.save') }}</button>
					</div>
				</div>
			</div>
		</form>
	<!--end::Card footer-->
	</div>
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection


{{-- Scripts Section --}}
@push('scripts')
	@foreach (\App\Models\Setting::where('type', 'file')->get() as $setting)
		<script>
			new KTImageInput('{{ $setting->keyword }}');
		</script>
	@endforeach
@endpush
