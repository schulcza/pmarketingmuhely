@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.roles.inputs.name')</h5>
                    <span>{{ $role->name ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('roles.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('common.back')
                </a>

                <a href="{{ route('roles.create') }}" class="btn btn-light">
                    {{ Metronic::getSVG('assets/images/svg/icons/Navigation/Plus.svg', 'svg-icon svg-icon-md') }}
					@lang('common.' . ($editing ? 'update' : 'save'))
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
