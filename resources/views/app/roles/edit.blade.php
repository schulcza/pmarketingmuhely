@extends('layouts.app')

@section('content')
<div class="card card-custom gutter-b">
	<div class="card-body">
		<x-form
			method="PUT"
			action="{{ route('dashboard.roles.update', $role) }}"
			class="mt-4"
		>
			@include('app.roles.form-inputs')

			<div class="mt-4">
				<a href="{{ route('dashboard.roles.index') }}" class="btn btn-light">
					{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Arrow-left.svg', 'svg-icon svg-icon-md') }}
					@lang('common.back')
				</a>

				<a href="{{ route('dashboard.roles.create') }}" class="btn btn-light">
					<i class="icon ion-md-add text-primary"></i>
					@lang('common.create')
				</a>

				<button type="submit" class="btn btn-primary float-right">
					{{ Metronic::getSVG('assets/images/svg/icons/General/Save.svg', 'svg-icon svg-icon-md') }}
					@lang('common.update')
				</button>
			</div>
		</x-form>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
