@extends('layouts.app')

@section('content')
{{-- Delete modal --}}
@include('app.partials.delete-modal')

<div class="card card-custom gutter-b">
	<div class="card-body">
		<div class="searchbar mt-4 mb-5">
			<div class="row">
				<div class="col-md-8">
					<form>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<input
										id="indexSearch"
										type="text"
										name="search"
										placeholder="{{ __('common.search') }}"
										value="{{ $search ?? '' }}"
										class="form-control"
										autocomplete="off"
									/>
									<div class="input-group-append">
										<button
											type="submit"
											class="btn btn-primary"
										>
											{{ Metronic::getSVG('assets/images/svg/icons/General/Search.svg', 'svg-icon svg-icon-md') }}
										</button>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<a
									href="{{ route('dashboard.roles.index') }}"
									class="btn btn-primary ml-2"
								>
									{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Close.svg', 'svg-icon svg-icon-md') }}
								</a>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4 text-right">
					<a
						href="{{ route('dashboard.roles.create') }}"
						class="btn btn-primary"
					>
						{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Plus.svg', 'svg-icon svg-icon-md') }}
						@lang('common.create')
					</a>
				</div>
			</div>
		</div>

		<div class="table-responsive">
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>
							@lang('common.name')
						</th>
						<th class="text-center">
							@lang('common.actions')
						</th>
					</tr>
				</thead>
				<tbody>
					@forelse($roles as $role)
					<tr>
						<td>{{ $role->name ?? '-' }}</td>
						<td class="text-center" style="width: 134px;">
							<div
								role="group"
								aria-label="Row Actions"
								class="btn-group"
							>
								<a href="{{ route('dashboard.roles.edit', $role) }}" class="btn btn-sm btn-light-warning btn-text-primary btn-hover-primary btn-icon mr-2" title="">
									{{ Metronic::getSVG('assets/images/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md') }}
								</a>

								<a onclick="showDeleteModal('{{ $role->id }}', '{{ \Request::getPathInfo() }}/')" data-toggle="modal" data-target="#delete-modal" data-placement="top" href="" class="btn btn-sm btn-light-danger btn-text-primary btn-hover-primary btn-icon mr-2" title="">
									{{ Metronic::getSVG('assets/images/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md') }}
								</a>
							</div>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="2">
							@lang('common.no_items_found')
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
