@extends('layouts.app')

@section('content')
<div class="card card-custom gutter-b">
	<div class="card-body">
		<x-form
			method="POST"
			action="{{ route('dashboard.roles.store') }}"
			class="mt-4"
		>
			@include('app.roles.form-inputs')

			<div class="mt-4">
				<a href="{{ route('dashboard.roles.index') }}" class="btn btn-light">
					{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Arrow-left.svg', 'svg-icon svg-icon-md') }}
					@lang('common.back')
				</a>

				<button type="submit" class="btn btn-primary float-right">
					{{ Metronic::getSVG('assets/images/svg/icons/General/Save.svg', 'svg-icon svg-icon-md') }}
					@lang('common.create')
				</button>
			</div>
		</x-form>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
