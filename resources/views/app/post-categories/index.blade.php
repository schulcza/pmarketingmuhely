@extends('layouts.app')

@section('content')
{{-- Delete modal --}}
@include('app.partials.delete-modal')

<div class="card card-custom gutter-b" id="post-categories">
	<div class="card-body">
		<div class="searchbar mt-4 mb-5">
			<div class="row">
				<div class="col-md-8">
					<form>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<input
										id="search"
										type="text"
										name="search"
										placeholder="{{ __('common.search') }}"
										value="{{ $search ?? '' }}"
										class="form-control"
										autocomplete="off"
									/>
									<div class="input-group-append">
										<button
											type="submit"
											class="btn btn-primary"
										>
											{{ Metronic::getSVG('assets/images/svg/icons/General/Search.svg', 'svg-icon svg-icon-md') }}
										</button>
									</div>
									
								</div>
							</div>
							<div class="col-md-2">
								<a
									href="{{ route('dashboard.post-categories.index') }}"
									class="btn btn-primary ml-2"
								>
									{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Close.svg', 'svg-icon svg-icon-md') }}
								</a>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4 text-right">
					<a
						href="{{ route('dashboard.post-categories.create') }}"
						class="btn btn-primary"
					>
						{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Plus.svg', 'svg-icon svg-icon-md') }}
						@lang('common.create')
					</a>

				</div>
			</div>
		</div>

		<div class="table-responsive">
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>
							@sortablelink('name', __('post-categories.name'))
						</th>
						<th>
							@sortablelink('created_at', __('common.created_at'))
						</th>	
						<th class="text-center">
							@lang('common.actions')
						</th>
					</tr>
				</thead>
				<tbody>
					@forelse($postCategories as $postCategory)
					<tr>
						<td>{{ $postCategory->name ?? '-' }}</td>
						<td>{{ dateTimeFormat($postCategory->created_at) }}</td>
						<td class="text-center" style="width: 134px;">
							<div
								role="group"
								aria-label="Row Actions"
								class="btn-group"
							>
								<a aria-label="Edit" data-balloon-pos="up"  href="{{ route('dashboard.post-categories.edit', $postCategory) }}" class="btn btn-sm btn-light-warning btn-text-primary btn-hover-primary btn-icon mr-2" title="">
									{{ Metronic::getSVG('assets/images/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md') }}
								</a>

								<a aria-label="Delete" data-balloon-pos="up" onclick="showDeleteModal('{{ $postCategory->id }}', '{{ \Request::getPathInfo() }}/')" data-toggle="modal" data-target="#delete-modal" data-placement="top" href="" class="btn btn-sm btn-light-danger btn-text-primary btn-hover-primary btn-icon mr-2" title="">
									{{ Metronic::getSVG('assets/images/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md') }}
								</a>										
							</div>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="8">
							@lang('common.no_items_found')
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
			{{ $postCategories->appends(request()->query())->links('app.partials.pagination') }}
		</div>
	</div>
</div>
@endsection