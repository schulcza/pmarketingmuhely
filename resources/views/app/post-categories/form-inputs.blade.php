<div class="row">
	<x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="{{ __('post-categories.name') }}"
            value="{{ old('name', ($editing ? $postCategory->name : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
