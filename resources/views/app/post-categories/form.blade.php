@php $editing = isset($postCategory) @endphp

@extends('layouts.app')

@section('content')
<div class="card card-custom gutter-b">
	<div class="card-body">
		<x-form
			method="POST"
			action="{{ route('dashboard.post-categories.store', $postCategory ?? null) }}"
			class="mt-4"
			enctype="multipart/form-data"
		>
			@include('app.post-categories.form-inputs')

			<div class="mt-4">
				<a href="{{ route('dashboard.post-categories.index') }}" class="btn btn-light">
					{{ Metronic::getSVG('assets/images/svg/icons/Navigation/Arrow-left.svg', 'svg-icon svg-icon-md') }}
					@lang('common.back')
				</a>

				<button type="submit" class="btn btn-primary float-right">
					{{ Metronic::getSVG('assets/images/svg/icons/General/Save.svg', 'svg-icon svg-icon-md') }}
					@lang('common.' . ($editing ? 'update' : 'save'))
				</button>
			</div>
		</x-form>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
