function showNotification(message, type, delay = 5000, allow_dismiss = true) {
    $.notify({
        message: message
    }, {
        type: type,
        delay: delay,
        allow_dismiss: allow_dismiss,
    });
}

function showDeleteModal(id, url) {
    $("#delete-modal input[name=id]").val(id);
    $("#delete-modal form").attr('action', url + id);
    // $("#delete-modal").modal();
}

function showEnableModal(id, url) {
    $("#question-enable-modal input[name=id]").val(id);
    $("#question-enable-modal form").attr('action', url + id + '/enable');
    $("#question-enable-modal").modal();
}

function showDisableModal(id, url) {
    $("#question-disable-modal input[name=id]").val(id);
    $("#question-disable-modal form").attr('action', url + id + '/disable');
    $("#question-disable-modal").modal();
}