<?php

return [
    'permissions' => [
        'base' => [
            'read dashboard',
            'read settings',

            'create users',
            'read users',
            'update users',
            'delete users',

            'create roles',
            'read roles',
            'update roles',
            'delete roles'
        ],
        'modules' => [
            'create projects',
            'read projects',
            'update projects',
            'delete projects',

            'create project categories',
            'read project categories',
            'update project categories',
            'delete project categories',

            'create services',
            'read services',
            'update services',
            'delete services',

            'create posts',
            'read posts',
            'update posts',
            'delete posts',

            'create post categories',
            'read post categories',
            'update post categories',
            'delete post categories'
        ]
    ]
];
