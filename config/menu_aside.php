<?php
// Aside menu
return [

    'items' => [
        [
            'title' => 'Dashboard',
            'icon' => 'assets/images/svg/icons/Design/Layers.svg',
            'root' => true,
            'page' => 'dashboard.index',
            'bullet' => 'line',
            'permission' => ['read dashboard']
        ],
        [
            'section' => 'Project',
        ],
        [
            'title' => 'Projects',
            'icon' => 'assets/images/svg/icons/Devices/Laptop.svg',
            'root' => true,
            'page' => 'dashboard.projects.index',
            'new-tab' => false,
            'permission' => ['read projects'],
            'secondary-pages' => [
                [
                    'title' => 'Create project',
                    'page' => 'dashboard.projects.create'
                ],
                [
                    'title' => 'Edit project',
                    'page' => 'dashboard.projects.edit'
                ]
            ]
        ],
        [
            'title' => 'Project Categories',
            'icon' => 'assets/images/svg/icons/Shopping/Price1.svg',
            'root' => true,
            'page' => 'dashboard.project-categories.index',
            'new-tab' => false,
            'permission' => ['read project categories'],
            'secondary-pages' => [
                [
                    'title' => 'Create project',
                    'page' => 'dashboard.project-categories.create'
                ],
                [
                    'title' => 'Edit project',
                    'page' => 'dashboard.project-categories.edit'
                ]
            ]
        ],
        [
            'section' => 'Service',
        ],
        [
            'title' => 'Services',
            'icon' => 'assets/images/svg/icons/General/Smile.svg',
            'root' => true,
            'page' => 'dashboard.services.index',
            'new-tab' => false,
            'permission' => ['read services'],
            'secondary-pages' => [
                [
                    'title' => 'Create service',
                    'page' => 'dashboard.services.create'
                ],
                [
                    'title' => 'Edit service',
                    'page' => 'dashboard.services.edit'
                ]
            ]
        ],
		[
            'section' => 'Blog',
        ],
        [
            'title' => 'Posts',
            'icon' => 'assets/images/svg/icons/Files/File.svg',
            'root' => true,
            'page' => 'dashboard.posts.index',
            'new-tab' => false,
            'permission' => ['read posts'],
            'secondary-pages' => [
                [
                    'title' => 'Create post',
                    'page' => 'dashboard.posts.create'
                ],
                [
                    'title' => 'Edit project',
                    'page' => 'dashboard.posts.edit'
                ]
            ]
        ],
        [
            'title' => 'Post Categories',
            'icon' => 'assets/images/svg/icons/Shopping/Price1.svg',
            'root' => true,
            'page' => 'dashboard.post-categories.index',
            'new-tab' => false,
            'permission' => ['read post categories'],
            'secondary-pages' => [
                [
                    'title' => 'Create post categry',
                    'page' => 'dashboard.post-categories.create'
                ],
                [
                    'title' => 'Edit post category',
                    'page' => 'dashboard.post-categories.edit'
                ]
            ]
        ],
        [
            'section' => 'User managaement',
        ],
        [
            'title' => 'Users',
            'icon' => 'assets/images/svg/icons/Communication/Group.svg',
            'root' => true,
            'page' => 'dashboard.users.index',
            'new-tab' => false,
            'permission' => ['view users'],
            'secondary-pages' => [
                [
                    'title' => 'Create user',
                    'page' => 'dashboard.users.create'
                ],
                [
                    'title' => 'Edit user',
                    'page' => 'dashboard.users.edit'
                ]
            ]
        ],
        [
            'section' => 'Settings',
        ],
        [
            'title' => 'Base settings',
            'icon' => 'assets/images/svg/icons/General/Settings-1.svg',
            'root' => true,
            'page' => 'dashboard.settings.index',
            'new-tab' => false,
            'permission' => ['read settings'],
            'secondary-pages' => []

        ],
        [
            'title' => 'Roles',
            'icon' => 'assets/images/svg/icons/General/Settings-1.svg',
            'root' => true,
            'page' => 'dashboard.roles.index',
            'new-tab' => false,
            'permission' => ['read settings'],
            'secondary-pages' => [
                [
                    'title' => 'Edit role',
                    'page' => 'dashboard.roles.edit'
                ],
                [
                    'title' => 'Create role',
                    'page' => 'dashboard.roles.create'
                ]
            ],
        ],

    ]

];
