<?php

return [
    [
        'name' => 'Weboldal készítés',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
    [
        'name' => 'Logó és arculattervezés',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
    [
        'name' => 'Social média management',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
    [
        'name' => 'Google Ads',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
    [
        'name' => 'Videó gyártás és szerkesztés',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
    [
        'name' => 'Fitózás, termékfotózás',
        'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos.',
        'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, quia optio. Deserunt reprehenderit maiores ut expedita eos. ',
        'icon' => 'icon-speedometer'
    ],
];
