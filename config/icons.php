<?php

return [
    [
        'name' => 'user',
        'value' => 'icon-user'
    ],
    [
        'name' => 'people',
        'value' => 'icon-people'
    ],
    [
        'name' => 'user-female',
        'value' => 'icon-user-female'
    ],
    [
        'name' => 'user-follow',
        'value' => 'icon-user-follow'
    ],
    [
        'name' => 'user-following',
        'value' => 'icon-user-following'
    ],
    [
        'name' => 'user-unfollow',
        'value' => 'icon-user-unfollow'
    ],
    [
        'name' => 'login',
        'value' => 'icon-login'
    ],
    [
        'name' => 'logout',
        'value' => 'icon-logout'
    ],
    [
        'name' => 'emotsmile',
        'value' => 'icon-emotsmile'
    ],
    [
        'name' => 'phone',
        'value' => 'icon-phone'
    ],
    [
        'name' => 'call-end',
        'value' => 'icon-call-end'
    ],
    [
        'name' => 'call-in',
        'value' => 'icon-call-in'
    ],
    [
        'name' => 'call-out',
        'value' => 'icon-call-out'
    ],
    [
        'name' => 'map',
        'value' => 'icon-map'
    ],
    [
        'name' => 'location-pin',
        'value' => 'icon-location-pin'
    ],
    [
        'name' => 'direction',
        'value' => 'icon-direction'
    ],
    [
        'name' => 'directions',
        'value' => 'icon-directions'
    ],
    [
        'name' => 'compass',
        'value' => 'icon-compass'
    ],
    [
        'name' => 'layers',
        'value' => 'icon-layers'
    ],
    [
        'name' => 'menu',
        'value' => 'icon-menu'
    ],
    [
        'name' => 'list',
        'value' => 'icon-list'
    ],
    [
        'name' => 'options-vertical',
        'value' => 'icon-options-vertical'
    ],
    [
        'name' => 'options',
        'value' => 'icon-options'
    ],
    [
        'name' => 'arrow-down',
        'value' => 'icon-arrow-down'
    ],
    [
        'name' => 'arrow-left',
        'value' => 'icon-arrow-left'
    ],
    [
        'name' => 'arrow-right',
        'value' => 'icon-arrow-right'
    ],
    [
        'name' => 'arrow-up',
        'value' => 'icon-arrow-up'
    ],
    [
        'name' => 'arrow-up-circle',
        'value' => 'icon-arrow-up-circle'
    ],
    [
        'name' => 'arrow-left-circle',
        'value' => 'icon-arrow-left-circle'
    ],
    [
        'name' => 'arrow-right-circle',
        'value' => 'icon-arrow-right-circle'
    ],
    [
        'name' => 'arrow-down-circle',
        'value' => 'icon-arrow-down-circle'
    ],
    [
        'name' => 'check',
        'value' => 'icon-check'
    ],
    [
        'name' => 'clock',
        'value' => 'icon-clock'
    ],
    [
        'name' => 'plus',
        'value' => 'icon-plus'
    ],
    [
        'name' => 'minus',
        'value' => 'icon-minus'
    ],
    [
        'name' => 'close',
        'value' => 'icon-close'
    ],
    [
        'name' => 'event',
        'value' => 'icon-event'
    ],
    [
        'name' => 'exclamation',
        'value' => 'icon-exclamation'
    ],
    [
        'name' => 'organization',
        'value' => 'icon-organization'
    ],
    [
        'name' => 'trophy',
        'value' => 'icon-trophy'
    ],
    [
        'name' => 'screen-smartphone',
        'value' => 'icon-screen-smartphone'
    ],
    [
        'name' => 'screen-desktop',
        'value' => 'icon-screen-desktop'
    ],
    [
        'name' => 'plane',
        'value' => 'icon-plane'
    ],
    [
        'name' => 'notebook',
        'value' => 'icon-notebook'
    ],
    [
        'name' => 'mustache',
        'value' => 'icon-mustache'
    ],
    [
        'name' => 'mouse',
        'value' => 'icon-mouse'
    ],
    [
        'name' => 'magnet',
        'value' => 'icon-magnet'
    ],
    [
        'name' => 'energy',
        'value' => 'icon-energy'
    ],
    [
        'name' => 'disc',
        'value' => 'icon-disc'
    ],
    [
        'name' => 'cursor',
        'value' => 'icon-cursor'
    ],
    [
        'name' => 'cursor-move',
        'value' => 'icon-cursor-move'
    ],
    [
        'name' => 'crop',
        'value' => 'icon-crop'
    ],
    [
        'name' => 'chemistry',
        'value' => 'icon-chemistry'
    ],
    [
        'name' => 'speedometer',
        'value' => 'icon-speedometer'
    ],
    [
        'name' => 'shield',
        'value' => 'icon-shield'
    ],
    [
        'name' => 'screen-tablet',
        'value' => 'icon-screen-tablet'
    ],
    [
        'name' => 'magic-wand',
        'value' => 'icon-magic-wand'
    ],
    [
        'name' => 'hourglass',
        'value' => 'icon-hourglass'
    ],
    [
        'name' => 'graduation',
        'value' => 'icon-graduation'
    ],
    [
        'name' => 'ghost',
        'value' => 'icon-ghost'
    ],
    [
        'name' => 'game-controller',
        'value' => 'icon-game-controller'
    ],
    [
        'name' => 'fire',
        'value' => 'icon-fire'
    ],
    [
        'name' => 'eyeglass',
        'value' => 'icon-eyeglass'
    ],
    [
        'name' => 'envelope-open',
        'value' => 'icon-envelope-open'
    ],
    [
        'name' => 'envelope-letter',
        'value' => 'icon-envelope-letter'
    ],
    [
        'name' => 'bell',
        'value' => 'icon-bell'
    ],
    [
        'name' => 'badge',
        'value' => 'icon-badge'
    ],
    [
        'name' => 'anchor',
        'value' => 'icon-anchor'
    ],
    [
        'name' => 'wallet',
        'value' => 'icon-wallet'
    ],
    [
        'name' => 'vector',
        'value' => 'icon-vector'
    ],
    [
        'name' => 'speech',
        'value' => 'icon-speech'
    ],
    [
        'name' => 'puzzle',
        'value' => 'icon-puzzle'
    ],
    [
        'name' => 'printer',
        'value' => 'icon-printer'
    ],
    [
        'name' => 'present',
        'value' => 'icon-present'
    ],
    [
        'name' => 'playlist',
        'value' => 'icon-playlist'
    ],
    [
        'name' => 'pin',
        'value' => 'icon-pin'
    ],
    [
        'name' => 'picture',
        'value' => 'icon-picture'
    ],
    [
        'name' => 'handbag',
        'value' => 'icon-handbag'
    ],
    [
        'name' => 'globe-alt',
        'value' => 'icon-globe-alt'
    ],
    [
        'name' => 'globe',
        'value' => 'icon-globe'
    ],
    [
        'name' => 'folder-alt',
        'value' => 'icon-folder-alt'
    ],
    [
        'name' => 'folder',
        'value' => 'icon-folder'
    ],
    [
        'name' => 'film',
        'value' => 'icon-film'
    ],
    [
        'name' => 'feed',
        'value' => 'icon-feed'
    ],
    [
        'name' => 'drop',
        'value' => 'icon-drop'
    ],
    [
        'name' => 'drawer',
        'value' => 'icon-drawer'
    ],
    [
        'name' => 'docs',
        'value' => 'icon-docs'
    ],
    [
        'name' => 'doc',
        'value' => 'icon-doc'
    ],
    [
        'name' => 'diamond',
        'value' => 'icon-diamond'
    ],
    [
        'name' => 'cup',
        'value' => 'icon-cup'
    ],
    [
        'name' => 'calculator',
        'value' => 'icon-calculator'
    ],
    [
        'name' => 'bubbles',
        'value' => 'icon-bubbles'
    ],
    [
        'name' => 'briefcase',
        'value' => 'icon-briefcase'
    ],
    [
        'name' => 'book-open',
        'value' => 'icon-book-open'
    ],
    [
        'name' => 'basket-loaded',
        'value' => 'icon-basket-loaded'
    ],
    [
        'name' => 'basket',
        'value' => 'icon-basket'
    ],
    [
        'name' => 'bag',
        'value' => 'icon-bag'
    ],
    [
        'name' => 'action-undo',
        'value' => 'icon-action-undo'
    ],
    [
        'name' => 'action-redo',
        'value' => 'icon-action-redo'
    ],
    [
        'name' => 'wrench',
        'value' => 'icon-wrench'
    ],
    [
        'name' => 'umbrella',
        'value' => 'icon-umbrella'
    ],
    [
        'name' => 'trash',
        'value' => 'icon-trash'
    ],
    [
        'name' => 'tag',
        'value' => 'icon-tag'
    ],
    [
        'name' => 'support',
        'value' => 'icon-support'
    ],
    [
        'name' => 'frame',
        'value' => 'icon-frame'
    ],
    [
        'name' => 'size-fullscreen',
        'value' => 'icon-size-fullscreen'
    ],
    [
        'name' => 'size-actual',
        'value' => 'icon-size-actual'
    ],
    [
        'name' => 'shuffle',
        'value' => 'icon-shuffle'
    ],
    [
        'name' => 'share-alt',
        'value' => 'icon-share-alt'
    ],
    [
        'name' => 'share',
        'value' => 'icon-share'
    ],
    [
        'name' => 'rocket',
        'value' => 'icon-rocket'
    ],
    [
        'name' => 'question',
        'value' => 'icon-question'
    ],
    [
        'name' => 'pie-chart',
        'value' => 'icon-pie-chart'
    ],
    [
        'name' => 'pencil',
        'value' => 'icon-pencil'
    ],
    [
        'name' => 'note',
        'value' => 'icon-note'
    ],
    [
        'name' => 'loop',
        'value' => 'icon-loop'
    ],
    [
        'name' => 'home',
        'value' => 'icon-home'
    ],
    [
        'name' => 'grid',
        'value' => 'icon-grid'
    ],
    [
        'name' => 'graph',
        'value' => 'icon-graph'
    ],
    [
        'name' => 'microphone',
        'value' => 'icon-microphone'
    ],
    [
        'name' => 'music-tone-alt',
        'value' => 'icon-music-tone-alt'
    ],
    [
        'name' => 'music-tone',
        'value' => 'icon-music-tone'
    ],
    [
        'name' => 'earphones-alt',
        'value' => 'icon-earphones-alt'
    ],
    [
        'name' => 'earphones',
        'value' => 'icon-earphones'
    ],
    [
        'name' => 'equalizer',
        'value' => 'icon-equalizer'
    ],
    [
        'name' => 'like',
        'value' => 'icon-like'
    ],
    [
        'name' => 'dislike',
        'value' => 'icon-dislike'
    ],
    [
        'name' => 'control-start',
        'value' => 'icon-control-start'
    ],
    [
        'name' => 'control-rewind',
        'value' => 'icon-control-rewind'
    ],
    [
        'name' => 'control-play',
        'value' => 'icon-control-play'
    ],
    [
        'name' => 'control-pause',
        'value' => 'icon-control-pause'
    ],
    [
        'name' => 'control-forward',
        'value' => 'icon-control-forward'
    ],
    [
        'name' => 'control-end',
        'value' => 'icon-control-end'
    ],
    [
        'name' => 'volume-1',
        'value' => 'icon-volume-1'
    ],
    [
        'name' => 'volume-2',
        'value' => 'icon-volume-2'
    ],
    [
        'name' => 'volume-off',
        'value' => 'icon-volume-off'
    ],
    [
        'name' => 'calendar',
        'value' => 'icon-calendar'
    ],
    [
        'name' => 'bulb',
        'value' => 'icon-bulb'
    ],
    [
        'name' => 'chart',
        'value' => 'icon-chart'
    ],
    [
        'name' => 'ban',
        'value' => 'icon-ban'
    ],
    [
        'name' => 'bubble',
        'value' => 'icon-bubble'
    ],
    [
        'name' => 'camrecorder',
        'value' => 'icon-camrecorder'
    ],
    [
        'name' => 'camera',
        'value' => 'icon-camera'
    ],
    [
        'name' => 'cloud-download',
        'value' => 'icon-cloud-download'
    ],
    [
        'name' => 'cloud-upload',
        'value' => 'icon-cloud-upload'
    ],
    [
        'name' => 'envelope',
        'value' => 'icon-envelope'
    ],
    [
        'name' => 'eye',
        'value' => 'icon-eye'
    ],
    [
        'name' => 'flag',
        'value' => 'icon-flag'
    ],
    [
        'name' => 'heart',
        'value' => 'icon-heart'
    ],
    [
        'name' => 'info',
        'value' => 'icon-info'
    ],
    [
        'name' => 'key',
        'value' => 'icon-key'
    ],
    [
        'name' => 'link',
        'value' => 'icon-link'
    ],
    [
        'name' => 'lock',
        'value' => 'icon-lock'
    ],
    [
        'name' => 'lock-open',
        'value' => 'icon-lock-open'
    ],
    [
        'name' => 'magnifier',
        'value' => 'icon-magnifier'
    ],
    [
        'name' => 'magnifier-add',
        'value' => 'icon-magnifier-add'
    ],
    [
        'name' => 'magnifier-remove',
        'value' => 'icon-magnifier-remove'
    ],
    [
        'name' => 'paper-clip',
        'value' => 'icon-paper-clip'
    ],
    [
        'name' => 'paper-plane',
        'value' => 'icon-paper-plane'
    ],
    [
        'name' => 'power',
        'value' => 'icon-power'
    ],
    [
        'name' => 'refresh',
        'value' => 'icon-refresh'
    ],
    [
        'name' => 'reload',
        'value' => 'icon-reload'
    ],
    [
        'name' => 'settings',
        'value' => 'icon-settings'
    ],
    [
        'name' => 'star',
        'value' => 'icon-star'
    ],
    [
        'name' => 'symbol-female',
        'value' => 'icon-symbol-female'
    ],
    [
        'name' => 'symbol-male',
        'value' => 'icon-symbol-male'
    ],
    [
        'name' => 'target',
        'value' => 'icon-target'
    ],
    [
        'name' => 'credit-card',
        'value' => 'icon-credit-card'
    ],
    [
        'name' => 'paypal',
        'value' => 'icon-paypal'
    ],
    [
        'name' => 'social-tumblr',
        'value' => 'icon-social-tumblr'
    ],
    [
        'name' => 'social-twitter',
        'value' => 'icon-social-twitter'
    ],
    [
        'name' => 'social-facebook',
        'value' => 'icon-social-facebook'
    ],
    [
        'name' => 'social-instagram',
        'value' => 'icon-social-instagram'
    ],
    [
        'name' => 'social-linkedin',
        'value' => 'icon-social-linkedin'
    ],
    [
        'name' => 'social-pinterest',
        'value' => 'icon-social-pinterest'
    ],
    [
        'name' => 'social-github',
        'value' => 'icon-social-github'
    ],
    [
        'name' => 'social-google',
        'value' => 'icon-social-google'
    ],
    [
        'name' => 'social-reddit',
        'value' => 'icon-social-reddit'
    ],
    [
        'name' => 'social-skype',
        'value' => 'icon-social-skype'
    ],
    [
        'name' => 'social-dribbble',
        'value' => 'icon-social-dribbble'
    ],
    [
        'name' => 'social-behance',
        'value' => 'icon-social-behance'
    ],
    [
        'name' => 'social-foursqare',
        'value' => 'icon-social-foursqare'
    ],
    [
        'name' => 'social-soundcloud',
        'value' => 'icon-social-soundcloud'
    ],
    [
        'name' => 'social-spotify',
        'value' => 'icon-social-spotify'
    ],
    [
        'name' => 'social-stumbleupon',
        'value' => 'icon-social-stumbleupon'
    ],
    [
        'name' => 'social-youtube',
        'value' => 'icon-social-youtube'
    ],
    [
        'name' => 'social-dropbox',
        'value' => 'icon-social-dropbox'
    ],
    [
        'name' => 'social-vkontakte',
        'value' => 'icon-social-vkontakte'
    ],
    [
        'name' => 'social-steam',
        'value' => 'icon-social-steam'
    ]
];
