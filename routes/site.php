<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Site\HomeController;
use App\Http\Controllers\Site\PostController;
use App\Http\Controllers\Site\ContactController;
use App\Http\Controllers\Site\ProjectController;
use App\Http\Controllers\Site\ServiceController;

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/rolunk', [HomeController::class, 'about'])->name('about');

Route::get('/szolgaltatasok', [ServiceController::class, 'services'])->name('services');
Route::get('/szolgaltatasok/{slug}', [ServiceController::class, 'show'])->name('services.show');

Route::get('/referenciak', [ProjectController::class, 'index'])->name('projects.index');
Route::get('/referenciak/{slug}', [ProjectController::class, 'show'])->name('projects.show');

Route::get('/blog', [PostController::class, 'index'])->name('posts.index');
Route::get('/blog/{slug}', [PostController::class, 'show'])->name('posts.show');

Route::get('/kapcsolat', [ContactController::class, 'show'])->name('contact');
Route::get('/ajanlatkeres', [ContactController::class, 'request'])->name('request');
