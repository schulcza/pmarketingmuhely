<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
require __DIR__ . '/site.php';

Route::group(['middleware' => ['auth']], function () {
    require __DIR__ . '/app/dashboard.php';
    require __DIR__ . '/app/settings.php';
    require __DIR__ . '/app/profile.php';
    require __DIR__ . '/app/users.php';
    require __DIR__ . '/app/projects.php';
    require __DIR__ . '/app/project-categories.php';
    require __DIR__ . '/app/posts.php';
    require __DIR__ . '/app/post-categories.php';
    require __DIR__ . '/app/services.php';
    require __DIR__ . '/app/roles-permissions.php';
});
