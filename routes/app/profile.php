<?php

use App\Http\Controllers\App\ProfileController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
	Route::prefix('dashboard/profile')->name('dashboard.profile.')->group(function () {	
		Route::get('/', [ProfileController::class, 'getProfile'])->name('show');
		Route::post('/', [ProfileController::class, 'updateProfile'])->name('update');
		Route::prefix('change-password')->name('password.')->group(function () {	
			Route::get('/', [ProfileController::class, 'getPasswordChangePage'])->name('show');
			Route::post('/', [ProfileController::class, 'updatePassword'])->name('update');
		});
	});
});