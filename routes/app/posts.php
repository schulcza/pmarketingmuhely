<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\PostController;

Route::group(['middleware' => ['can:read posts']], function () {
	Route::prefix('dashboard/posts')->name('dashboard.posts.')->group(function () {	
		Route::get('/', [PostController::class, 'index'])->name('index');
		Route::get('/create', [PostController::class, 'create'])->name('create');
		Route::post('/{post?}', [PostController::class, 'store'])->name('store');
		Route::get('/{post}/edit', [PostController::class, 'edit'])->name('edit');
		Route::delete('/{post}', [PostController::class, 'destroy'])->name('destroy');
	});
});
