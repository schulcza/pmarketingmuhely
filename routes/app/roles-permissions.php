<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\RoleController;
use App\Http\Controllers\App\PermissionController;

Route::group(['middleware' => ['can:read settings']], function () {
    Route::prefix('dashboard/roles')->name('dashboard.roles.')->group(function () {
        Route::get('', [RoleController::class, 'index'])->name('index');
        Route::get('/create', [RoleController::class, 'create'])->name('create');
        Route::post('', [RoleController::class, 'store'])->name('store');
        Route::get('/{role}/edit', [RoleController::class, 'edit'])->name('edit');
        Route::put('/{role}', [RoleController::class, 'update'])->name('update');
        Route::delete('/{role}', [RoleController::class, 'destroy'])->name('destroy');
    });

    Route::resource('permissions', PermissionController::class);
});
