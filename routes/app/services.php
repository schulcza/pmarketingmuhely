<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\ServiceController;

Route::group(['middleware' => ['can:read services']], function () {
	Route::prefix('dashboard/services')->name('dashboard.services.')->group(function () {
		Route::get('/', [ServiceController::class, 'index'])->name('index');
		Route::get('/create', [ServiceController::class, 'create'])->name('create');
		Route::post('/{service?}', [ServiceController::class, 'store'])->name('store');
		Route::get('/{service}/edit', [ServiceController::class, 'edit'])->name('edit');
		Route::delete('/{service}', [ServiceController::class, 'destroy'])->name('destroy');
	});
});
