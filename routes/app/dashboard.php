<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\DashboardController;

Route::group(['middleware' => ['can:read dashboard']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
});
