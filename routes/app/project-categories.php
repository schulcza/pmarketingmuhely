<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\ProjectCategoryController;

Route::group(['middleware' => ['can:read services']], function () {
	Route::prefix('dashboard/project-categories')->name('dashboard.project-categories.')->group(function () {
		Route::get('/', [ProjectCategoryController::class, 'index'])->name('index');
		Route::get('/create', [ProjectCategoryController::class, 'create'])->name('create');
		Route::post('/{projectCategory?}', [ProjectCategoryController::class, 'store'])->name('store');
		Route::get('/{projectCategory}/edit', [ProjectCategoryController::class, 'edit'])->name('edit');
		Route::delete('/{projectCategory}', [ProjectCategoryController::class, 'destroy'])->name('destroy');
	});
});
