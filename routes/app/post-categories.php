<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\PostCategoryController;

Route::group(['middleware' => ['can:read post categories']], function () {
    Route::prefix('dashboard/post-categories')->name('dashboard.post-categories.')->group(function () {
        Route::get('/', [PostCategoryController::class, 'index'])->name('index');
        Route::get('/create', [PostCategoryController::class, 'create'])->name('create');
        Route::post('/{postCategory?}', [PostCategoryController::class, 'store'])->name('store');
        Route::get('/{postCategory}/edit', [PostCategoryController::class, 'edit'])->name('edit');
        Route::delete('/{postCategory}', [PostCategoryController::class, 'destroy'])->name('destroy');
    });
});
