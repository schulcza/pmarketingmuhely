<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\ProjectController;

Route::group(['middleware' => ['can:read projects']], function () {
	Route::prefix('dashboard/projects')->name('dashboard.projects.')->group(function () {	
		Route::get('/', [ProjectController::class, 'index'])->name('index');
		Route::get('/create', [ProjectController::class, 'create'])->name('create');
		Route::post('/{project?}', [ProjectController::class, 'store'])->name('store');
		Route::get('/{project}/edit', [ProjectController::class, 'edit'])->name('edit');
		Route::delete('/{project}', [ProjectController::class, 'destroy'])->name('destroy');
	});
});
