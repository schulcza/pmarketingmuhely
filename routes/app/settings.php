<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App\SettingController;

Route::group(['middleware' => ['can:read settings']], function () {
    Route::prefix('settings')->name('dashboard.settings.')->group(function () {
        Route::get('/', [SettingController::class, 'index'])->name('index');
        Route::post('/store', [SettingController::class, 'store'])->name('store');
    });
});
