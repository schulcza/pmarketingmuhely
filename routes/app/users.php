<?php

use App\Http\Controllers\App\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['can:view users']], function () {
    Route::prefix('dashboard/users')->name('dashboard.users.')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/create', [UserController::class, 'create'])->name('create');
        Route::post('/', [UserController::class, 'store'])->name('store');
        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('edit');
        Route::put('/{user}', [UserController::class, 'update'])->name('update');
        Route::delete('/{user}', [UserController::class, 'destroy'])->name('destroy');
    });
});
