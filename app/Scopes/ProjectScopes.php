<?php

namespace App\Scopes;

use Carbon\Carbon;

trait ProjectScopes
{
    public function scopeFeatured($query)
    {
        return $query->where('featured', true);
    }
}
