<?php

namespace App\Scopes;

use Carbon\Carbon;

trait PostScopes
{
    public function scopeFeatured($query)
    {
        return $query->where('featured', true);
    }
}
