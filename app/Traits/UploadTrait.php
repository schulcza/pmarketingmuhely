<?php

namespace App\Traits;

use File;

use App\Models\Image;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;

use Intervention\Image\ImageManagerStatic as LaravelImage;

trait UploadTrait
{
    /**
     * Upload images
     *
     * @param  \App\Request  $reguest
     * @return \Illuminate\Http\Response
     */

    public function uploadImage($file, $model_id, $modelName, $temporary = false)
    {
        $folder = strtolower($modelName);
        $folder =  Str::plural($folder);

        if ($temporary) {
            $sub_folder = 'temp_' . $model_id;
        } else {
            $sub_folder = $model_id;
        }

        $uniqid = uniqid();
        $filename = $uniqid . '.' . $file->getClientOriginalExtension();

        foreach (config('app.image_versions') as $ver => $img_ver) {
            $image = LaravelImage::make($file);

            $path = '/uploads/' . $folder . '/' . $sub_folder . '/'. $ver;

            $this->createFolders($path);
            $image->resize($img_ver['max_width'], null, function ($constraint) {
                $constraint->aspectRatio();
            });

            // $image->insert(public_path('/assets/site/images/watermark.png'), 'bottom-left', 15, 15);

            $image->save(public_path($path . '/' . $filename), config('app.max_image_quality'));
        }

        if (!$temporary) {
            $image_model = new Image;
            $image_model->imageable_id = $model_id;
            $image_model->imageable_type = 'App\Models\\' . ucfirst($modelName) ;
            $image_model->filename = $filename;
            $image_model->save();
        }

        return [
            'path' => $path . '/' . $filename,
            'filename' => $filename,
            'image_model' => $image_model ?? null,
            'temp' => $temporary,
        ];
    }

    public function createFolders($input)
    {
        $paths = explode('/', $input);
        $prev_path = public_path();

        foreach ($paths as $key => $path) {
            if (!file_exists($prev_path . '/' . $path)) {
                mkdir($prev_path . '/' . $path);
            }
            $prev_path = $prev_path . '/' . $path;
        }
    }

    public function deleteFiles($model)
    {
        $old_images = Image::where('imageable_type', get_class($model))
                            ->where('imageable_id', $model->id);

        if (Schema::hasColumn($model->getTable(), 'featured_image_id')) {
            $model->featured_image_id = null;
            $model->save();
        }

        $old_images->delete();

        $folder = Str::plural(strtolower(class_basename($model)));

        $path_to_delete = 'uploads/' . $folder . '/' . $model->id;

        File::deleteDirectory(public_path($path_to_delete));
    }

    public function deleteOneFile($image_id)
    {
        $image = Image::find($image_id);
        if ($image) {
            $folder = Str::plural(strtolower(class_basename($image->imageable)));

            foreach (config('app.image_versions') as $ver => $version) {
                $path_to_delete = 'uploads/' . $folder . '/' . $image->imageable_id . '/' . $ver . '/' . $image->filename;
                File::delete(public_path($path_to_delete));
            }
            $image->delete();
        }
    }
}
