<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Models\Scopes\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	use Searchable;
    use Sortable;

    protected $searchableFields = [
        'name',
        'excerpt',
        'body',
    ];

    public $sortable = [
        'id',
        'name',
        'excerpt',
        'created_at'
    ];

    protected $fillable = [
        'name',
        'excerpt',
        'body',
		'image_id',
        'icon'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->name);
        });
    }

    /**
     * Get image for the setting.
     *
     * @return String
     */
    public function getImage($type): String
    {
        $image = $this->belongsTo(Image::class, 'image_id')->first();

        if ($image) {
            return $image->getImage($type);
        }

        return config('app.image_placeholder');
    }
}
