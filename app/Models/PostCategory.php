<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Models\Scopes\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    use Searchable;
    use Sortable;

    protected $searchableFields = [
        'name'
    ];

    public $sortable = [
        'id',
        'name',
        'created_at'
    ];

    protected $fillable = [
        'name'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->name);
        });
    }
}
