<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Scopes\ProjectScopes;
use App\Models\Scopes\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Searchable;
    use Sortable;
    use ProjectScopes;

    protected $searchableFields = [
        'title',
        'body',
    ];

    public $sortable = [
        'id',
        'title',
        'created_at'
    ];

    protected $fillable = [
        'title',
        'body',
        'featured',
        'featured_image_id',
        'image_id',
        'url',
        'categories'
    ];

    protected $casts = [
        'categories' => 'array'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    /**
     * Get image for the setting.
     *
     * @return String
     */
    public function getImage($size, $type = 'featured_image'): String
    {
        $image = $this->belongsTo(Image::class, $type . '_id')->first();

        if ($image) {
            return $image->getImage($size);
        }

        return config('app.image_placeholder');
    }

    public function getProjectCategoriesAttribute()
    {
        $projectCategories = ProjectCategory::whereIn('id', $this->categories)->get();

        return $projectCategories;
    }

    public function getProjectCategoriesArrayAttribute()
    {
        $projectCategoriesArray = ProjectCategory::select('slug')
            ->whereIn('id', $this->categories)
            ->pluck('slug')
            ->toArray();

        return json_encode($projectCategoriesArray);
    }
}
