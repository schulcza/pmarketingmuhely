<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Scopes\PostScopes;
use App\Models\Scopes\Searchable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Searchable;
    use Sortable;
    use PostScopes;

    protected $searchableFields = [
        'title',
        'excerpt',
        'body',
    ];

    public $sortable = [
        'id',
        'title',
        'created_at'
    ];

    protected $fillable = [
        'title',
        'excerpt',
        'body',
        'featured',
        'featured_image_id',
        'categories'
    ];

    protected $casts = [
        'categories' => 'array'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    /**
     * Get image for the setting.
     *
     * @return String
     */
    public function getImage($size, $type = 'featured_image'): String
    {
        $image = $this->belongsTo(Image::class, $type . '_id')->first();

        if ($image) {
            return $image->getImage($size);
        }

        return config('app.image_placeholder');
    }

    public function getPostCategoriesAttribute()
    {
        $postCategories = PostCategory::whereIn('id', $this->categories)->get();

        return $postCategories;
    }
}
