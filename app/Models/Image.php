<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Str;

/**
 * Class Image
 *
 * @property int $id
 * @property int $imageable_id
 * @property string $imageable_type
 * @property string $filename
 * @property string $title
 * @property string $description
 * @property integer $order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property string $deletion_token
 * @property MorphTo $imageable
 */
class Image extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'order' => 'integer'
    ];

    /**
     * Get the owning imageable model.
     *
     * @return MorphTo
     */
    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }
    
    /**
     * Get the thumbnail of image
     *
     * @return String
     */
    public function getImage($type): String
    {
        return '/uploads/' . Str::plural(strtolower(class_basename($this->imageable_type))) . '/' . $this->imageable->id . '/' . $type . '/' . $this->filename;
    }
}
