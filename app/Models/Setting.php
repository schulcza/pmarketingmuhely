<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// use App\Models\Traits\CustomSoftDeletes;

class Setting extends Model
{
    // use CustomSoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'keyword',
        'value',
        'type',
        'group'
    ];

    /**
     * Get image for the setting.
     *
     * @return String
     */
    public function getImage($type): String
    {
        $image = $this->belongsTo(Image::class, 'value')->first();

        if ($image) {
            return $image->getImage($type);
        }

        return config('app.image_placeholder');
    }
}
