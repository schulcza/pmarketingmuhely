<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'image' => ['sometimes', 'file'],
            'featured_image' => ['sometimes', 'file'],
            'url' => ['sometimes', 'url'],
            'body' => ['required', 'min:8'],
            'featured' => ['required', 'boolean'],
            'categories' => ['required', 'array']
        ];
    }
}
