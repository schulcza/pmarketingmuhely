<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;

class SaveSettingsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation_array = [];
        $validation_rules = [
            'text' => 'max:255',
            'textarea' => 'sometimes',
            'checkbox' => 'sometimes',
            'number' => 'integer',
            'file' => 'sometimes:image'
        ];
        foreach (Setting::all() as $key => $setting) {
            $validation_array[$setting->keyword] = $validation_rules[$setting->type];
            if ($setting->type == 'file') {
                $validation_array[$setting->keyword . '_remove'] = 'sometimes:string';
            }
        }

        return $validation_array;
    }
}
