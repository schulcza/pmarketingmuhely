<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'excerpt' => ['required', 'string'],
            'featured_image' => ['sometimes', 'file'],
            'body' => ['required', 'min:8'],
            'featured' => ['required', 'boolean'],
            'categories' => ['required', 'array']
        ];
    }
}
