<?php

namespace App\Http\Controllers\App;

use App\Models\Setting;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\SaveSettingsRequest;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting_groups = Setting::get()->groupBy('group');

        return view('app.settings.index', compact('setting_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveSettingsRequest $request)
    {
        $validated = $request->validated();

        DB::beginTransaction();

        try {
            foreach ($validated as $key => $value) {
                $setting = Setting::where('keyword', $key)->first();

                if ($setting) {
                    if ($setting->type == 'file') {
                        $this->deleteFiles($setting);

                        $image_upload = $this->uploadImage($value, $setting->id, 'Setting', false);
                        $value = $image_upload['image_model']->id;

                        // if ($setting->keyword == 'og_image') {
                        // 	$facebook_helper = new FacebookHelper;
                        // 	$facebook_helper->ogDebug();
                        // }
                    }

                    $setting->update([
                        'value' => $value
                    ]);
                } else {
                    $file_settings = Setting::where('type', 'file')->get();

                    foreach ($file_settings as $key => $file_setting) {
                        $keyword = $file_setting->keyword;
                        if (array_key_exists($keyword . '_remove', $validated) && $validated[$keyword . '_remove']) {
                            $this->deleteFiles($file_setting);
                        }
                    }
                }
            }

            $checkbox_settings = Setting::where('type', 'checkbox')->get();

            foreach ($checkbox_settings as $checkbox_setting) {
                $checkbox_setting->update([
                    'value' => $request->input($checkbox_setting->keyword) ?? 'off'
                ]);
            }

            DB::commit();

            Cache::forever('settings', \App\Models\Setting::all());

            return redirect()
                    ->route('dashboard.settings.index')
                    ->with('success', __('common.saved'));
        } catch (\Exception $e) {
            return $this->handleException($e, __('common.unable_to_save'), true);
        }
    }
}
