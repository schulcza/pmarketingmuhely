<?php

namespace App\Http\Controllers\App;

use App\Models\ProjectCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectCategoryStoreRequest;

class ProjectCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $projectCategories = ProjectCategory::search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.project-categories.index', compact('projectCategories', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('app.project-categories.form');
    }

    /**
     * @param \App\Http\Requests\ProjectCategoryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectCategoryStoreRequest $request, ProjectCategory $projectCategory = null)
    {
        $validated = $request->validated();

        if ($projectCategory) {
            $projectCategory->update($validated);
        } else {
            $projectCategory = ProjectCategory::create($validated);
        }

        return redirect()
            ->route('dashboard.project-categories.edit', $projectCategory)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectCategory $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProjectCategory $projectCategory)
    {
        return view('app.project-categories.form', compact('projectCategory'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectCategory $projectCategory)
    {
        $projectCategory->delete();

        return redirect()
            ->route('dashboard.project-categories.index')
            ->withSuccess(__('common.removed'));
    }
}
