<?php

namespace App\Http\Controllers\App;

use App\Models\Post;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Models\PostCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostStoreRequest;

class PostController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $posts = Post::search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.posts.index', compact('posts', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $postCategories = PostCategory::get();

        return view('app.posts.form', compact('postCategories'));
    }

    /**
     * @param \App\Http\Requests\UserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request, Post $post = null)
    {
        $validated = $request->validated();

        if ($post) {
            $post->update($validated);
        } else {
            $post = Post::create($validated);
        }

        if (array_key_exists('featured_image', $validated)) {
            $this->deleteOneFile($post->featured_image_id);
            $image_upload = $this->uploadImage($validated['featured_image'], $post->id, 'Post');
            $value = $image_upload['image_model']->id;

            $post->update([
                'featured_image_id' => $value
            ]);
        }

        return redirect()
            ->route('dashboard.posts.edit', $post)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Post $post)
    {
        $postCategories = PostCategory::get();

        return view('app.posts.form', compact('post', 'postCategories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        $post->delete();

        return redirect()
            ->route('dashboard.posts.index')
            ->withSuccess(__('common.removed'));
    }
}
