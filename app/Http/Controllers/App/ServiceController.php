<?php

namespace App\Http\Controllers\App;

use App\Models\Service;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceStoreRequest;

class ServiceController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $services = Service::search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.services.index', compact('services', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('app.services.form');
    }

    /**
     * @param \App\Http\Requests\ServiceStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceStoreRequest $request, Service $service = null)
    {
        $validated = $request->validated();

        if ($service) {
            $service->update($validated);
        } else {
            $service = Service::create($validated);
        }

        if (array_key_exists('image', $validated)) {
            $this->deleteOneFile($service->image_id);
            $image_upload = $this->uploadImage($validated['image'], $service->id, 'Service');
            $value = $image_upload['image_model']->id;

            $service->update([
                'image_id' => $value
            ]);
        }

        return redirect()
            ->route('dashboard.services.edit', $service)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Service $service)
    {
        return view('app.services.form', compact('service'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Service $service)
    {
        $service->delete();

        return redirect()
            ->route('dashboard.services.index')
            ->withSuccess(__('common.removed'));
    }
}
