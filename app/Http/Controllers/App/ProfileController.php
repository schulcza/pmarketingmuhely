<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\SaveProfileRequest;
use App\Http\Requests\SavePasswordRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile()
    {
        $user = \Auth::user();
        return view('app.profile.show', compact([
            'user'
        ]));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPasswordChangePage()
    {
        $user = \Auth::user();
        return view('app.profile.change-password', compact([
            'user'
        ]));
    }
    
    /**
     * Save user profile base data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(SaveProfileRequest $request)
    {
        $validated = $request->validated();

        DB::beginTransaction();

        try {
            $user = \Auth::user();
            $user->update($validated);

            DB::commit();

            if (array_key_exists('profile_image', $validated) || array_key_exists('profile_image_remove', $validated) && $validated['profile_image_remove']) {
                $this->deleteFiles($user);
            }

            if (array_key_exists('profile_image', $validated)) {
                $image_upload = $this->uploadImage($validated['profile_image'], $user->id, 'User', false);

                $user->update([
                    'profile_image_id' => $image_upload['image_model']->id
                ]);
            }

            return redirect()
                    ->route('dashboard.profile.show')
                    ->with('success', __('common.saved'));
        } catch (\Exception $e) {
            return $this->handleException($e, __('common.unable_to_save'), true);
        }
    }

    /**
     * Update the user's password
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(SavePasswordRequest $request)
    {
        $validated = $request->validated();

        try {
            $validated['password'] = Hash::make($validated['password']);

            DB::beginTransaction();

            \Auth::user()->update($validated);

            DB::commit();

            return redirect()
                    ->route('dashboard.profile.password.show')
                    ->with('success', __('common.saved'));
        } catch (\Exception $e) {
            return $this->handleException($e, __('common.unable_to_delete'), true);
        }
    }
}
