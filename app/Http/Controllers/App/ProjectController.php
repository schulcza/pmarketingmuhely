<?php

namespace App\Http\Controllers\App;

use App\Models\Project;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Models\ProjectCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectStoreRequest;

class ProjectController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $projects = Project::search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.projects.index', compact('projects', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $projectCategories = ProjectCategory::get();

        return view('app.projects.form', compact('projectCategories'));
    }

    /**
     * @param \App\Http\Requests\UserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request, Project $project = null)
    {
        $validated = $request->validated();

        if ($project) {
            $project->update($validated);
        } else {
            $project = Project::create($validated);
        }

        if (array_key_exists('featured_image', $validated)) {
            $this->deleteOneFile($project->featured_image_id);
            $image_upload = $this->uploadImage($validated['featured_image'], $project->id, 'Project');
            $value = $image_upload['image_model']->id;

            $project->update([
                'featured_image_id' => $value
            ]);
        }

        if (array_key_exists('image', $validated)) {
            $this->deleteOneFile($project->image_id);
            $image_upload = $this->uploadImage($validated['image'], $project->id, 'Project');
            $value = $image_upload['image_model']->id;

            $project->update([
                'image_id' => $value
            ]);
        }

        return redirect()
            ->route('dashboard.projects.edit', $project)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project)
    {
        $projectCategories = ProjectCategory::get();

        return view('app.projects.form', compact('project', 'projectCategories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Project $project)
    {
        $project->delete();

        return redirect()
            ->route('dashboard.projects.index')
            ->withSuccess(__('common.removed'));
    }
}
