<?php

namespace App\Http\Controllers\App;

use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostCategoryStoreRequest;

class PostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $postCategories = PostCategory::search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.post-categories.index', compact('postCategories', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('app.post-categories.form');
    }

    /**
     * @param \App\Http\Requests\PostCategoryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCategoryStoreRequest $request, PostCategory $postCategory = null)
    {
        $validated = $request->validated();

        if ($postCategory) {
            $postCategory->update($validated);
        } else {
            $postCategory = PostCategory::create($validated);
        }

        return redirect()
            ->route('dashboard.post-categories.edit', $postCategory)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PostCategory $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, PostCategory $postCategory)
    {
        return view('app.post-categories.form', compact('postCategory'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PostCategory $postCategory)
    {
        $postCategory->delete();

        return redirect()
            ->route('dashboard.post-categories.index')
            ->withSuccess(__('common.removed'));
    }
}
