<?php

namespace App\Http\Controllers\App;

use App\Models\User;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use App\Exports\AllUsersExport;
use App\Imports\UsersPointsImport;
use Spatie\Permission\Models\Role;

use App\Exports\UserByPointsExport;

use App\Exports\InactiveUsersExport;

use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Exports\LastDayAnsweredUsersExport;
use App\Exports\LastSevenDayAnsweredUsersExport;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $users = User::select(
            [
                'users.*'
            ]
        );

        $users = $users->search($search)
            ->sortable('created_at')
            ->paginate(config('app.pagination_limit'));

        return view('app.users.index', compact('users', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = Role::get();

        return view('app.users.create', compact('roles'));
    }

    /**
     * @param \App\Http\Requests\UserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $validated = $request->validated();

        $validated['password'] = Hash::make($validated['password']);

        $user = User::create($validated);

        $user->syncRoles($request->roles);

        return redirect()
            ->route('users.edit', $user)
            ->withSuccess(__('common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        return view('app.users.show', compact('user'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        $roles = Role::get();

        return view('app.users.edit', compact('user', 'roles'));
    }

    /**
     * @param \App\Http\Requests\UserStoreRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserStoreRequest $request, User $user)
    {
        $validated = $request->validated();

        $user->update($validated);

        $user->syncRoles($request->roles);

        return redirect()
            ->route('dashboard.users.edit', $user)
            ->withSuccess(__('common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return redirect()
            ->route('users.index')
            ->withSuccess(__('common.removed'));
    }
}
