<?php

namespace App\Http\Controllers\Site;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function show($slug)
    {
        $service = Service::where('slug', $slug)->first();
        $services = Service::all();

        return view('site.services.show', compact(
            'service',
			'services'
        ));
    }
}
