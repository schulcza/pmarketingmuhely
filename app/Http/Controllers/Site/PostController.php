<?php

namespace App\Http\Controllers\Site;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Models\ProjectCategory;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('site.posts.index', compact(
            'posts'
        ));
    }

    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        $postCategories = PostCategory::all();

        return view('site.posts.show', compact(
            'post',
            'postCategories'
        ));
    }
}
