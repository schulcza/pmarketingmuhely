<?php

namespace App\Http\Controllers\Site;

use App\Models\Post;
use App\Models\Project;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $services = Service::all();
        $posts = Post::featured()->take(8)->get();
        $projects = Project::take(6)->get();

        return view('site.home', compact(
            'services',
            'projects',
            'posts'
        ));
    }

    public function about()
    {
        return view('site.about');
    }
}
