<?php

namespace App\Http\Controllers\Site;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectCategory;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::all();
        $projectCategories = ProjectCategory::all();

        return view('site.projects.index', compact(
            'projects',
            'projectCategories'
        ));
    }

    public function show($slug)
    {
        $project = Project::where('slug', $slug)->first();
        $projectCategories = ProjectCategory::all();

        return view('site.projects.show', compact(
            'project',
            'projectCategories'
        ));
    }
}
