<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function handleException($e, string $message, ?bool $rollback = false)
    {
        if ($rollback) {
            DB::rollback();
        }

        \Log::error($message, ['error' => $e]);

        if (config('app.debug')) {
            throw $e;
        }

        return redirect()
                ->back()
                ->with('error', $message);
    }
}
