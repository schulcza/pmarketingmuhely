<?php
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

use App\Models\Poll;

if (!function_exists('dateTimeFormat')) {
    /**
     * Get status name by status code
     *
     * @param integer $status
     * @return string
     */
    function dateTimeFormat($date): String
    {
        return \Carbon\Carbon::parse($date)->format(config('app.date_time_format'));
    }
}

if (!function_exists('dateTimeFormatFull')) {
    /**
     * Get status name by status code
     *
     * @param integer $status
     * @return string
     */
    function dateTimeFormatFull($date): String
    {
        return \Carbon\Carbon::parse($date)->format(config('app.date_time_format_full'));
    }
}

if (!function_exists('dateFormat')) {
    /**
     * Get status name by status code
     *
     * @param integer $status
     * @return string
     */
    function dateFormat($date): String
    {
        return \Carbon\Carbon::parse($date)->format(config('app.date_format'));
    }
}

if (!function_exists('getIntendedUrl')) {
    /**
     * @param string $default
     * @return string
     */
    function getIntendedUrl($default): string
    {
        return session()->pull('url.intended')[0] ?? $default;
    }
}

if (!function_exists('setIntendedUrl')) {
    /**
     * @return void
     */
    function setIntendedUrl(): void
    {
        $intended = request()->method() === 'GET' && request()->route() && ! request()->expectsJson()
                        ? url()->full()
                        : url()->previous();
                        
        if ($intended) {
            session()->push('url.intended', $intended);
        }
    }
}

if (!function_exists('menuIsActive')) {
    function menuIsActive($route)
    {
        if ($route == \Route::currentRouteName()) {
            return 'active';
        }
    }
}


if (!function_exists('settings')) {
    function settings($keyword = null, $fallback_value = null)
    {
        if (Schema::hasTable('settings')) {
            $settings = Cache::rememberForever('settings', function () {
                return \DB::table('settings')->get();
            });

            if ($keyword) {
                $setting = $settings->where('keyword', $keyword)->first();
                if ($setting) {
                    if ($setting->type == 'file' && !empty($setting->value) && !is_null($setting->value)) {
                        return $setting->getImage('normal') !== '' ? $setting->getImage('normal') : $fallback_value;
                    }
                    return $setting->value;
                }
            }

            return $settings;
        }
    }
}
